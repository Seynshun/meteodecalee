# ##################################################
#
# ELVEX
#
# Copyright 2019 LABRI, 
# CNRS (UMR 5800), the University of Bordeaux,
# and the Bordeaux INP
#
# Author: 
# Lionel Clément
# LaBRI -- Université Bordeaux 
# 351, cours de la Libération
# 33405 Talence Cedex - France
# lionel.clement@labri.fr
# 
# This file is part of ELVEX.
#
################################################## #

make -s
(cd data/lefff ; make -s -j7)

cd data

echo "--------------------------------------------------------"


#../src/elvex -r -compactLexiconDirectory lefff -compactLexiconFile lefff -grammarFile pdp-meteo.grammar -lexiconFile pdp-meteo.local-lexicon -inputFile pdp-meteo.input | sed 's/  */ /g' | ../src/elvexpostedition | tr '@' "\n" |sed -e 's/^ *//' |tr '@' "\n"

../src/elvex -compactLexiconDirectory lefff -compactLexiconFile lefff -grammarFile pdp-meteo.grammar -lexiconFile pdp-meteo.local-lexicon -inputFile pdp-meteo.input | sed 's/  */ /g' | ../src/elvexpostedition | tr '@' "\n" |sed -e 's/^ *//' |tr '@' "\n"

#../src/elvexdebug --traceShift -compactLexiconDirectory lefff -compactLexiconFile lefff -grammarFile pdp-meteo.grammar -lexiconFile pdp-meteo.local-lexicon -inputFile pdp-meteo.input

cd ..
