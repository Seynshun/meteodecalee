# Météo Décalée

Android app for display weather reports with humorous/jargon sentence using the text generator [Elvex](https://github.com/lionelclement/Elvex) (written by Lionel Clément)  for the Programming Project course of Master 1 of the University of Bordeaux.

<p align="center">
<img alt="terminal" src="https://zupimages.net/up/20/39/pqau.jpg" height="350" />
<img alt="graphical interface" src="https://zupimages.net/up/20/39/54vi.jpg" height="350"/>
<img alt="graphical interface" src="https://zupimages.net/up/20/39/dzo2.jpg" height="350"/>
</p>

# Features

- Display a sentence describing the day's weather according to its GPS position.
- Possibility to select another city from which you want to receive the weather forecast.
- 5-day weather forecast.
- Temperature evolution every 3 hours.
- Minimum and maximum temperature per day.

# Architecture


## Back End

The application has a back end part that retrieves weather reports from APIs and parses this information to pass it as input to the Elvex text generator to obtain a sentence related to the weather of the day.

## Front End
The front end corresponds to the application available on the smartphone, it is connected to the server to receive phrases and weather reports at startup.