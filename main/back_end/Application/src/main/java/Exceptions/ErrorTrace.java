package Exceptions;


import java.util.ArrayList; 

/**
 * It is used to trace all errors during the sentence generation process 
 * from the creation of the client to the send of the forecast to the application. 
 * There's one ErrorTrace per ClientHandler.
 * 
 */

public class ErrorTrace implements Cloneable{
    
    private ArrayList<String> _warnings;
    private String _error = null;

    public ErrorTrace(){
        _warnings = new ArrayList<String>();
    }

    public void addWarning(String warning){
        if(!_warnings.contains(warning))
            _warnings.add(warning);
    }

    public void setError(String error){
        _error = error;
    }

    public void clearWarnings(){
        _warnings.clear();
    }

    public ArrayList<String> getWarnings(){
        return new ArrayList<String>(_warnings);
    }

    public String getError(){
        return _error;
    }

    public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException cnse) {
			cnse.printStackTrace(System.err);
		}
		return o;
	}

    public String toString(){
        String result = "";
        if(_error != null){
            result += "Il y a 1 erreur: "+_error+"\n";
        }
        result += "Voici les warnings rencontres:\n";
        for(String warning : getWarnings()){
            result += warning +"\n";
        }
        return result;
    }

}