import java.io.IOException;

import TextGenerator.*;
import Request.*;
import Server.*;
public class Main {
	 public static void main(String[] args) throws IOException{  
		

		WeatherAPI apiRequest=new OpenWeatherMapRequest();
		RunTextGenerator runTextGenerator = new RunElvexV2();
		
		ServerTCP serverTCP = new ServerTCP(apiRequest, runTextGenerator);
		serverTCP.startServer();
	}
}
