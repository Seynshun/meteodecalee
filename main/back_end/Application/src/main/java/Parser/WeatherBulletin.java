package Parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import Exceptions.ErrorTrace;

/**
 * WeatherBulletin will store importants informations used to generate sentence.
 * 
 */
public class WeatherBulletin {
    private final double MIN_TEMP = -50;
    private final double MAX_TEMP = 50;
    private final double MIN_HUMIDITY = 0;
    private final double MAX_HUMIDITY = 101;
    private final double MIN_WINDSPEED = 0;
    private final double MAX_WINDSPEED = 120;
    private final double IMPOSSIBLE_VALUE=200;
    private final double MAX_LATITUDE = 91;
    private final double MIN_LATITUDE = -91;
    private final double MAX_LONGITUDE = 181;
    private final double MIN_LONGITUDE = -181;
    private final double MIN_PRECIPITATION = 0.0;
    private final double MAX_PRECIPITATION = 30;
    private final double MIN_SNOWVOLUME = 0.0;
    private final double MAX_SNOWVOLUME = 800;


    private double _temperature;
    private double _minTemperature=IMPOSSIBLE_VALUE;
    private double _maxTemperature=IMPOSSIBLE_VALUE;
    private double _humidity=IMPOSSIBLE_VALUE;
    private double _precipitation=0.0;
    private double _snowVolume=0.0;
    private double _windSpeed;
    private double _latitude;
    private double _longitude;
    private String _cityname;
    private String _country;
    private String _dateFrom;
    private String _dateTo;
    private LinkedHashMap<String,Double> _forecastWeather;

    /**
     * This _erroTrace variable is used to check values validity.
     */
    private ErrorTrace _errorTrace; 

    public WeatherBulletin(ErrorTrace errorTrace){
        _errorTrace = errorTrace;
    }
    
    public void setForecastWeather(LinkedHashMap<String,Double> forecastWeather){
        _forecastWeather = new LinkedHashMap<String,Double>(forecastWeather);
    }

    public void setDateFrom(String date) throws ParseException{
    	//check if date pattern is matching to existing date
        if(!(isDateInGoodFormat (date))) {
            _errorTrace.setError("la date n'est pas dans le bon format"+ date);
        } 
        
        //check if date is a correct date
        if(!(isACorrectDate(date))) {
            _errorTrace.setError("la date est complement erronee, exemple le mois = 13,"+ date);
        }
        
        //check if date is not before today date 
        if(isDateBeforeTodayDate(date)) {
            _errorTrace.setError("la date est erronee"+ date);
        }
        _dateFrom=date;
    }
    
    private boolean isDateInGoodFormat(String date) {
    	//check if date pattern is matching to existing date
        if(!(Pattern.matches("\\d{4}-\\d{2}-\\d{2}",date) || Pattern.matches("\\d{4}-\\d{1}-\\d{1}", date) 
        		|| Pattern.matches("\\d{4}-\\d{1}-\\d{2}", date) || Pattern.matches("\\d{4}-\\d{2}-\\d{1}", date))){
            return false;
        } 
        return true;
    }
    
    private boolean isACorrectDate(String date) {
        //check if date is really existing
    	//Generating today date
		final Calendar cal = Calendar.getInstance();
        String[] dateSplitted = date.split("-");
        int dateYear = Integer.parseInt(dateSplitted[0]);
        int dateMonth = Integer.parseInt(dateSplitted[1]);
        int dateDay = Integer.parseInt(dateSplitted[2]);
        if(dateYear < Year.now().getValue() || dateYear > Year.now().getValue()+1 ||
        		dateMonth <= 0 || dateMonth >= 13 || dateDay <= 0 || dateDay >= 32) {
        	return false;
        } else {
        	return true;
        }
    }
    
    private boolean isDateBeforeTodayDate(String date) throws ParseException {
        //check if dateTo is before today date
    	//Generating today date
		final Calendar cal = Calendar.getInstance();
        String[] dateSplitted = date.split("-");
        String dateYear = dateSplitted[0];
        String dateMonth = dateSplitted[1];
        String dateDay = dateSplitted[2];
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date todayDate = sdf.parse(Year.now().getValue() + "-" + (cal.get(Calendar.MONTH)+1) + "-" + cal.get(Calendar.DAY_OF_MONTH));
        Date compareDate = sdf.parse(dateYear+"-"+dateMonth+"-"+dateDay);
        
        if (compareDate.before(todayDate)) {
        	return true;
        } else {
        	return false;
        }
    }
    
    public void setDateTo(String date) throws ParseException{
    	//check if date pattern is matching to existing date
        if(!(isDateInGoodFormat (date))) {
            _errorTrace.setError("la date n'est pas dans le bon format"+ date);
        } 
        
        //check if date is a correct date
        if(!(isACorrectDate(date))) {
            _errorTrace.setError("la date est complement erronee, exemple le mois = 13"+ date);
        }
        
        //check if date is not before today date 
        if(isDateBeforeTodayDate(date)) {
            _errorTrace.setError("la date est erronee"+ date);
        }
        _dateTo=date;
    }
    public void setTemperature(double temp){
        if(temp < MIN_TEMP || temp >MAX_TEMP){
            _errorTrace.setError("les températures sont probablements fausses"+ temp);
        }
        _temperature = temp;
    }
    public void setMinTemperature(double temp){
        if(temp < MIN_TEMP || temp >MAX_TEMP){
            _errorTrace.setError("les temperatures sont probablements fausses"+ temp);
        }
        _minTemperature = temp;
    }
    public void setMaxTemperature(double temp){
        if(temp < MIN_TEMP || temp >MAX_TEMP){
            _errorTrace.setError("les temperatures sont probablements fausses"+ temp);
        }
        _maxTemperature = temp;
    }
    public void setHumidity(double humidity){
        if(humidity < MIN_HUMIDITY || humidity >MAX_HUMIDITY){
            _errorTrace.setError("l'humidite est probablement fausse"+ humidity);
        }
        _humidity = humidity;
    }
    public void setWindSpeed(double windSpeed){
        if(windSpeed < MIN_WINDSPEED || windSpeed >MAX_WINDSPEED){
            _errorTrace.setError("la vitesse du vent est probablement fausse."+ windSpeed);
        }
        _windSpeed = windSpeed;
    }
    public void setCoordonates(double latitude,double longitude){
        if(latitude < MIN_LATITUDE || latitude > MAX_LATITUDE){
            _errorTrace.setError("la latitude est probablement fausse."+ latitude);
        }
        if(longitude < MIN_LONGITUDE || longitude > MAX_LONGITUDE){
            _errorTrace.setError("la longitude est probablement fausse."+ longitude);
        }
        _longitude= longitude;
        _latitude= latitude;

    }
    public void setPrecipitation(double precipitation){
        if(precipitation < MIN_PRECIPITATION || precipitation > MAX_PRECIPITATION){
            _errorTrace.setError("la precipitation est probablement fausse."+ precipitation);
        }
        _precipitation = precipitation;
    }
    
    public void setCityName(String cityName){
        if(cityName == null){
            _errorTrace.setError("la ville est inconnue"+ cityName);
        }
        _cityname = cityName;
    }
    public void setCountry(String country){
        if(country == null){
            _errorTrace.setError("le pays est inconnu"+ country);
        }
        _country = country;
    }
    public void setSnowVolume(double snowVolume){
        if(snowVolume < MIN_SNOWVOLUME || snowVolume >= MAX_SNOWVOLUME){
            _errorTrace.setError("le volume de neige est probablement faux."+ snowVolume);
        }
        _snowVolume = snowVolume;
    }

    public String getDateFrom(){
        return this._dateFrom;
    }
    public String getDateTo(){
        return this._dateTo;
    }
    public double getTemperature(){
        return this._temperature;
    }
    public double getMinTemperature(){
        return this._minTemperature;
    }
    public double getMaxTemperature(){
        return this._maxTemperature;
    }

    public double getHumidity(){
        return this._humidity;
    }
    
    public double getWindSpeed(){
        return this._windSpeed;
    }
    
    public double getPrecipitation(){
        return this._precipitation;
    }
    
    public ErrorTrace getErrorTrace(){
        return this._errorTrace;
    }
    
    public String getCityName() {
        return this._cityname;
    }

    
    public String getCountry() {

        return this._country;
    }

    
    public double getLatitude() {
        return this._latitude;
    }

    
    public double getLongitude() {
        return this._longitude;
    }

    public double getSnowVolume() {
        return this._snowVolume;
    }


    public String toString(){
        String city_name = "A: "+ _cityname+"\n";
        String date = "Du "+ _dateFrom+ " au "+_dateTo+" \n";
        String temperature = "Il fait: "+ _temperature + " C\n";
        String minTemperature = "min: "+_minTemperature+ " C\n";
        String maxTemperature = "max: "+_maxTemperature+ " C\n";
        String windSpeed = "un vent a "+ _windSpeed + " m/s\n";
        String humidity = "l'humidite:  "+ _humidity+"%\n" ;
        String precipitation = "pluie:  "+ _precipitation+"mm\n" ;
        String snowVolume = "neige:  "+ _snowVolume+"mm\n" ;

        return city_name+date+temperature+minTemperature+maxTemperature+windSpeed+humidity+precipitation+snowVolume;
    }


    public JSONArray getForecastArray(){
        if(_forecastWeather==null){
            return null;
        }
        JSONArray list = new JSONArray();
        

        for (String i : _forecastWeather.keySet()) {
            JSONObject forecastTemperature = new JSONObject();
            forecastTemperature.put("date",i);
            forecastTemperature.put("temperature",_forecastWeather.get(i));
            list.put(forecastTemperature);
            
            
        }
        
        return list;
    }

    public JSONObject toJson(){
        JSONObject result = new JSONObject();
		result.put("city", _cityname);
        result.put("longitude", _longitude);
        result.put("latitude", _latitude);
        result.put("temperature", _temperature);
        result.put("temperature_max", _maxTemperature);
        result.put("temperature_min", _minTemperature);
        result.put("date",_dateFrom);
        return result;
    }

}