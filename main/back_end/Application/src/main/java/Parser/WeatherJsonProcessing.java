package Parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import Exceptions.ErrorTrace;

/**
 * This class is used by ClientHandler class.
 * it prepares the json that will be sent to the app.
 */
public class WeatherJsonProcessing {

	private void addErrorToJsonObject(JSONObject json, String error){
		json.accumulate("error", error);
	}

	private void addWarningsToJsonObject(JSONObject json, ArrayList<String> warnings ){

		JSONObject main = json.getJSONObject("main");

		JSONArray warningsList = new JSONArray();
		int nbWarnings = 0;

		for (String w : warnings){
			JSONObject warning_field = new JSONObject();
			warning_field.put("warning"+nbWarnings,w);
			warningsList.put(warning_field);
			nbWarnings++;
		}
	
		main.accumulate("nbWarnings", nbWarnings);
		json.put("warnings", warningsList);
		
	}

	/**
	 * Create the JSONObject that will be sent.
	 * @param sentences
	 * @param bulletins
	 * @param errorTrace
	 * @return the forecast with the sentence generated for each day.
	 * 
	 * Json structure:
	 * 
	 * {
	 * 	error:{"error message"},
	 * 	warnings:{ [ warnings array]},
	 * 	today:{
	 * 		"sentence":{ }
	 * 		"temperature":
	 * 	}
	 * 	main:{
	 * 		"nbForecast":
	 * 		"nbForecastList2":
	 * 		"city":
	 * 		"latitude":
	 * 		"longitude":
	 * 	}
	 * 	
	 * List:{[List of bulletin by day]}
	 * 
	 * List2: {[List of 3 hourly forecast by day]}
	 * }
	 */

	public JSONObject jsonBuilder(ArrayList<String> sentences, ArrayList<WeatherBulletin> bulletins,ErrorTrace errorTrace){
		JSONObject result = new JSONObject();
		JSONObject today = new JSONObject();
		JSONObject main = new JSONObject();
		JSONArray list = new JSONArray();
		result.put("main", main);
		if(!errorTrace.getWarnings().isEmpty()){
			addWarningsToJsonObject(result, errorTrace.getWarnings());
		}

		if(errorTrace.getError()!=null){
			addErrorToJsonObject(result, errorTrace.getError());
			return result;
		}
		
		
		
		JSONArray list2 = bulletins.get(0).getForecastArray();

		today.put("sentence", sentences.get(0));
		today.put("temperature", bulletins.get(0).getTemperature());

		main.put("nbForecast", bulletins.size()-1);
		main.put("nbForecastList2", list2.length());
		main.put("city",bulletins.get(0).getCityName());
		main.put("latitude",bulletins.get(0).getLatitude());
		main.put("longitude",bulletins.get(0).getLongitude());

		JSONObject forecast;

		for( int i = 0; i<bulletins.size(); i++ ){
			forecast = bulletins.get(i).toJson();
			forecast.put("sentence",sentences.get(i));
			list.put(forecast);
		}
		
		result.put("today", today);
		result.put("main", main);
		result.put("list", list);
		result.put("list2",list2);

		
		

		return result;
	}

	public JSONObject jsonBuilder(String sentence, ArrayList<WeatherBulletin> bulletins,ErrorTrace errorTrace){
		JSONObject result = new JSONObject();
		JSONObject today = new JSONObject();
		JSONObject main = new JSONObject();
		JSONArray list = new JSONArray();
		result.put("main", main);
		if(!errorTrace.getWarnings().isEmpty()){
			addWarningsToJsonObject(result, errorTrace.getWarnings());
		}

		if(errorTrace.getError()!=null){
			addErrorToJsonObject(result, errorTrace.getError());
			return result;
		}
		
		
		
		JSONArray list2 = bulletins.get(0).getForecastArray();

		today.put("sentence", sentence);
		today.put("temperature", bulletins.get(0).getTemperature());

		main.put("nbForecast", bulletins.size()-1);
		main.put("nbForecastList2", list2.length());
		main.put("city",bulletins.get(0).getCityName());
		main.put("latitude",bulletins.get(0).getLatitude());
		main.put("longitude",bulletins.get(0).getLongitude());

		JSONObject forecast;

		for( int i = 0; i<bulletins.size(); i++ ){
			forecast = bulletins.get(i).toJson();
			forecast.put("sentence","");
			list.put(forecast);
		}
		
		result.put("today", today);
		result.put("main", main);
		result.put("list", list);
		result.put("list2",list2);

		
		

		return result;
	}

	
}
