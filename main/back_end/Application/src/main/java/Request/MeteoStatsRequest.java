package Request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.json.JSONObject;
/**
 * @see https://api.meteostat.net/
 * @see https://www.meteostat.net/sources
 * 
 * Data provided by meteostat. 
 * Meteorological data: Copyright © National Oceanic and Atmospheric Administration (NOAA), 
 * Deutscher Wetterdienst (DWD). Learn more about the sources.
 * 
 * 
 */


public class MeteoStatsRequest extends NormalsData {
    /**
     * Used to make the connection between the months and their digital representation.
     * @see JSONObject averageSeason(JSONObject normals)
     */
    private Map<Integer, String> _months = new HashMap<>();


    /**
     * The key we use to make calls with the API
     */
    private String apiKey = "URaDQmzE";


    /**
     * The Station's ID can be recovered from the API by making a specific query:
     * 
     * https://api.meteostat.net/v1/stations/nearby?lat={lat}&lon={lon}&key={apiKey}
     * @see List<String> getNearbyStations(double latitude, double longitude)
     * 
     * By default we use the Bordeaux station's ID, because some stations don't provide data.
     */
    private String defaultStationId = "07510"; // ID de Bordeaux



    /**
     * This is used as a cache, and prevents the server from making the same calls.
     */
    private JSONObject savedNormalsData;

    public MeteoStatsRequest() {
        setMonths();
    }

    private String setConnection(String urlString) {
        if (_errorTrace.getError() != null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            // System.out.println(result);

        } catch (IOException e) {
            //errorMessage("Nous ne pouvons communiquer avec l'api 'MeteoStat'");
            return null;
        }

        return result.toString();
    }

    private void setMonths() {
        _months.put(1, "JAN");
        _months.put(2, "FEB");
        _months.put(3, "MAR");
        _months.put(4, "APR");
        _months.put(5, "MAY");
        _months.put(6, "JUN");
        _months.put(7, "JUL");
        _months.put(8, "AUG");
        _months.put(9, "SEP");
        _months.put(10, "NOV");
        _months.put(11, "OCT");
        _months.put(12, "DEC");
    }



    /**
     *  This method gets a list of stations near the coordinates.
     * @param latitude
     * @param longitude
     * @return A list of station id.
     */
    private List<String> getNearbyStations(double latitude, double longitude) {
        if (_errorTrace.getError() != null) {
            return null;
        }
        String url = "https://api.meteostat.net/v1/stations/nearby?lat=" + latitude + "&lon=" + longitude + "&key="
                + apiKey;
        List<String> nearbyStationsId = new ArrayList<String>();
        String response = setConnection(url);
        if (response == null) {
            //errorMessage("Nous ne pouvons communiquer avec l'API 'MeteoStat'");
            return null;
        }
        JSONObject jsonObject = new JSONObject(response);
        JSONArray station_array = (JSONArray) jsonObject.getJSONArray("data");
        for (int i = 0; i < station_array.length(); i++) {
            JSONObject station_field = station_array.getJSONObject(i);
            nearbyStationsId.add(station_field.getString("id"));
        }
        return nearbyStationsId;
    }

    private boolean checkJsonValidity(String json){
        JSONObject test = new JSONObject(json);
        JSONObject data_field = test.getJSONObject("data");
        JSONObject temperature_field = data_field.getJSONObject("temperature");
        JSONObject temperature_max_field = data_field.getJSONObject("temperature_max");
        JSONObject temperature_min_field = data_field.getJSONObject("temperature_min");
        JSONObject precipitation_field = data_field.getJSONObject("precipitation");
        JSONObject sunshine_field = data_field.getJSONObject("sunshine");
        JSONObject pressure_field = data_field.getJSONObject("precipitation");

        for (int i = 1; i < 13; i++){
            String m = _months.get(i);
            if(temperature_field.isNull(m) || temperature_min_field.isNull(m) ||temperature_max_field.isNull(m) 
                || sunshine_field.isNull(m) || pressure_field.isNull(m) || precipitation_field.isNull(m)){
                    
                return false;
            }
        }

        return true;
    }
    /**
     *  This method fills a specific HashMap with the climate normals based on coordonates
     * @param latitude
     * @param longitude
     * @return the climate normals in the HashMap.
     */
    public HashMap<String, Double> getNormalsFromMonth(int month, double latitude, double longitude) {
        if (_errorTrace.getError() != null) {
            return null;
        }
        HashMap<String, Double> normals = new HashMap<String, Double>();

        // check if it already in cache
        String response = "";
        if (savedNormalsData==null) {
            String nearbyStation = getValidStationId(latitude, longitude);
            String url = "https://api.meteostat.net/v1/climate/normals?station=" + nearbyStation + "&key=" + apiKey;
            response = setConnection(url);
            if (response == null) {
                warningMessage("Nous ne pouvons communiquer avec l'API 'MeteoStat'");
                savedNormalsData=averageSeason(new JSONObject(readLineByLineJava8("jsonFiles/bordeauxNormals.json")));
            }
            else{
                savedNormalsData=averageSeason(new JSONObject(response));       
            }
        }
        
        JSONObject data_field = savedNormalsData.getJSONObject(Integer.toString((int) month%3));
        double temperature= data_field.getDouble("temperature");
        double temperature_max= data_field.getDouble("temperature_max");
        double temperature_min= data_field.getDouble("temperature_min");
        double precipitation= data_field.getDouble("precipitation");
        double sunshine= data_field.getDouble("sunshine");
        double pressure= data_field.getDouble("precipitation");

        

        normals.put("temperature", temperature);
        normals.put("temperature_max", temperature_max);
        normals.put("temperature_min", temperature_min);
        normals.put("precipitation", precipitation);
        normals.put("sunshine", sunshine);
        normals.put("pressure", pressure);

        return normals;
    }



/**
 * Some of Station's Id can't provide data.
 * So we test each of them.
 * @param latitude
 * @param longitude
 * @return the first valid station's ID. Can return null, if there is no valid station.
 */
    private String getValidStationId(double latitude, double longitude) {
        if (_errorTrace.getError() != null) {
            return null;
        }
        List<String> nearbyStationsIDs = getNearbyStations(latitude, longitude);
        if(nearbyStationsIDs==null){
            return null;
        }
        for (String id : nearbyStationsIDs) {
            String url = "https://api.meteostat.net/v1/climate/normals?station=" + id + "&key=" + apiKey;
            String response = setConnection(url);
            if (response == null) {
                return null;
            }
            JSONObject jsonObject = new JSONObject(response);

            if (!jsonObject.isNull("data")) {
                if(checkJsonValidity(response)){
                    System.out.println("VALID");
                    return id;
                }
               
            }
        }
        warningMessage("La phrase générée est non représentative de la ville");
        return defaultStationId;
    }

/**
 * From climate normals by month, we calculate climate normals by seasons. 
 * @param normals
 * @return the climate normals by seasons.
 */
    private JSONObject averageSeason(JSONObject normals) {
        JSONObject result = new JSONObject();
        double averageTemp = 0;
        double averageMinTemp = 0;
        double averageMaxTemp = 0;
        double averageSunshine = 0;
        double averagePressure = 0;
        double averagePrecipitation = 0;
        JSONObject data_field = normals.getJSONObject("data");
        JSONObject temperature_field = data_field.getJSONObject("temperature");
        JSONObject temperature_max_field = data_field.getJSONObject("temperature_max");
        JSONObject temperature_min_field = data_field.getJSONObject("temperature_min");
        JSONObject precipitation_field = data_field.getJSONObject("precipitation");
        JSONObject sunshine_field = data_field.getJSONObject("sunshine");
        JSONObject pressure_field = data_field.getJSONObject("precipitation");
        int numSeason=0;
        for (int i = 1; i < 13; i++) {

            String m = _months.get(i);
            averageTemp += temperature_field.getDouble(m);
            averageMinTemp += temperature_min_field.getDouble(m);
            averageMaxTemp += temperature_max_field.getDouble(m);
            averageSunshine += sunshine_field.getDouble(m);
            averagePressure += pressure_field.getDouble(m);
            averagePrecipitation += precipitation_field.getDouble(m);

            if (i % 3 == 0) {
                JSONObject categorie = new JSONObject();
                
                categorie.put("temperature", averageTemp / 3);
                categorie.put("temperature_min", averageMinTemp / 3);
                categorie.put("temperature_max", averageMaxTemp / 3);
                categorie.put("precipitation", averagePrecipitation / 3);
                categorie.put("sunshine", averageSunshine / 3);
                categorie.put("pressure", averagePressure / 3);
                result.put(Integer.toString(numSeason),categorie);
                numSeason++;

                averageTemp = 0;
                averageMinTemp = 0;
                averageMaxTemp = 0;
                averageSunshine = 0;
                averagePressure = 0;
                averagePrecipitation = 0;

            }

        }
        return result;
    }

    private static String readLineByLineJava8(String filePath) 
{
    StringBuilder contentBuilder = new StringBuilder();
    try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8)) 
    {
        stream.forEach(s -> contentBuilder.append(s).append("\n"));
    }
    catch (IOException e) 
    {
        e.printStackTrace();
    }
    return contentBuilder.toString();
}
}