package Request;

import java.util.HashMap;

import Exceptions.ErrorTrace;

/**
 * This abstract describes the functions that a weather API must perform.
 * we can add a new API by creating a class extending this abstract.
 * 
 * This API is used to get climate normals.
 * With these normals,we can find out, for example, if it's colder or warmer than usual.
 * 
 */

public abstract class NormalsData implements Cloneable{

    public ErrorTrace _errorTrace;
    
    public abstract HashMap<String,Double> getNormalsFromMonth(int month, double latitude, double longitude);
    
    
    public Object clone(){
        Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException cnse) {
			cnse.printStackTrace(System.err);
		}
		return o;
    }
    
    public void errorMessage(String message){
        System.out.println(message);
        _errorTrace.setError(message);
    }

    public void warningMessage(String message){
        System.out.println(message);
        _errorTrace.addWarning(message);
    }

    /**
     * Set the errorTrace object to the generator.
     * See ErrorTrace description.
     * @param errorTrace
     */
    public void setErrorTrace(ErrorTrace errorTrace){
        this._errorTrace = errorTrace;
    }
}