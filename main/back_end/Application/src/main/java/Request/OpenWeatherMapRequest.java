package Request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import Parser.WeatherBulletin;

/**
 * OpenWeatherMapRequest use the Open Weather Api to extends WeatherAPI
 * Fonctions.
 * 
 * @see https://openweathermap.org/api
 */
public class OpenWeatherMapRequest extends WeatherAPI {

    private String apiKey = "84e306a4a65210f28cbfe764ad2566be";

    private String setUrl(String type, double longitude, double latitude) {
        String result = String.format(
                "http://api.openweathermap.org/data/2.5/%s?lat=%s&lon=%s&appid=%s&units=metric&lang=fr", type, latitude,
                longitude, apiKey);

        System.out.println(result);
        return result;
    }

    /**
     * From json received by the OpenWeather API, create a WeatherBulletin object
     * according to the weather.
     * 
     * @param jsonString : the json sent by the OpenWeather API.
     * 
     * @return a WeatherBulletin object filled from the json.
     * @throws ParseException
     */
    public WeatherBulletin setWeatherInformations(String jsonString) throws ParseException {
        WeatherBulletin bulletin = new WeatherBulletin(_errorTrace);
        JSONObject jsonObject = new JSONObject(jsonString);

        JSONObject wind_field = jsonObject.getJSONObject("wind");
        JSONObject main_field = jsonObject.getJSONObject("main");
        JSONObject sys_field = jsonObject.getJSONObject("sys");
        JSONObject coord_field = jsonObject.getJSONObject("coord");

        bulletin.setTemperature(main_field.getDouble("temp"));

        bulletin.setMinTemperature(main_field.getDouble("temp_min"));
        bulletin.setMaxTemperature(main_field.getDouble("temp_max"));
        bulletin.setCityName(jsonObject.getString("name"));
        bulletin.setHumidity(main_field.getDouble("humidity"));
        bulletin.setWindSpeed(wind_field.getDouble("speed"));

        bulletin.setCountry(sys_field.getString("country"));
        bulletin.setCoordonates(coord_field.getDouble("lat"), coord_field.getDouble("lon"));

        if (jsonObject.has("rain")) {
            JSONObject rain_field = jsonObject.getJSONObject("rain");
            if (rain_field.has("3h")) {
                bulletin.setPrecipitation(rain_field.getDouble("3h"));
            } else if (rain_field.has("1h")) {
                bulletin.setPrecipitation(rain_field.getDouble("1h"));
            }
        }

        if (jsonObject.has("snow")) {
            JSONObject snow_field = jsonObject.getJSONObject("snow");
            if (snow_field.has("3h")) {
                bulletin.setSnowVolume(snow_field.getDouble("3h"));
            } else if (snow_field.has("1h")) {
                bulletin.setSnowVolume(snow_field.getDouble("1h"));
            }
        }

        /*
         * We use the Calendar class, because the date class is depreciated
         */
        Calendar cal = Calendar.getInstance();
        String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        
        String day = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH) + 1)
                + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
        bulletin.setDateFrom(formattedDate);
        bulletin.setDateTo(formattedDate);
        System.out.println(bulletin.toString());
        return bulletin;

    }

    private WeatherBulletin setWeatherInformationsFromTimeInterval(String jsonString, Calendar from, Calendar to)
            throws ParseException {
        if (_errorTrace.getError() != null) {
            return null;
        }
        WeatherBulletin weatherBulletin = new WeatherBulletin(_errorTrace);
        setWeatherForecastTemperature(weatherBulletin, jsonString);

        JSONObject jsonObject = new JSONObject(jsonString);

        JSONArray days_array = jsonObject.getJSONArray("list");
        JSONObject city_field = jsonObject.getJSONObject("city");
        JSONObject coord_field = city_field.getJSONObject("coord");

        int nblines = jsonObject.getInt("cnt");
        double average_temp = 0;
        double average_humidity = 0;
        double min_temp = Double.MAX_VALUE;
        double max_temp = Double.MIN_VALUE;
        double average_windSpeed = 0;
        int nbHours = 0;
        int nbRainField = 0;
        int nbSnowField = 0;
        boolean findDay = false;
        double average_precipitation = 0;
        double average_snowVolume = 0;
        for (int i = 0; i < nblines; i++) {

            JSONObject hourlyWeather = days_array.getJSONObject(i);
            String tmp = hourlyWeather.getString("dt_txt");

            String[] dateString = tmp.split(" ");
            String[] dateStrings = dateString[0].split("-");
            Calendar cal = Calendar.getInstance();

            cal.set(Integer.parseInt(dateStrings[0]), Integer.parseInt(dateStrings[1]),
                    Integer.parseInt(dateStrings[2]));

            if (cal.get(Calendar.DAY_OF_YEAR) >= from.get(Calendar.DAY_OF_YEAR)
                    && cal.get(Calendar.YEAR) >= from.get(Calendar.YEAR)
                    && cal.get(Calendar.DAY_OF_YEAR) <= to.get(Calendar.DAY_OF_YEAR)
                    && cal.get(Calendar.YEAR) <= to.get(Calendar.YEAR)) {
                findDay = true;
                nbHours += 1;
                JSONObject wind_field = hourlyWeather.getJSONObject("wind");
                JSONObject main_field = hourlyWeather.getJSONObject("main");

                average_temp += main_field.getDouble("temp");
                double tmp_min_temp = main_field.getDouble("temp_min");
                double tmp_max_temp = main_field.getDouble("temp_max");
                if (min_temp > tmp_min_temp) {
                    min_temp = tmp_min_temp;
                }

                if (max_temp < tmp_max_temp) {
                    max_temp = tmp_max_temp;
                }

                average_humidity += main_field.getDouble("humidity");
                average_windSpeed += wind_field.getDouble("speed");
                if (hourlyWeather.has("rain")) {
                    JSONObject rain_field = hourlyWeather.getJSONObject("rain");
                    average_precipitation += rain_field.getDouble("3h");
                    nbRainField += 1;
                }

                if (hourlyWeather.has("snow")) {
                    JSONObject snow_field = hourlyWeather.getJSONObject("snow");
                    average_snowVolume += snow_field.getDouble("3h");
                    nbSnowField += 1;
                }
            } else if (findDay)
                break;

        }
        average_humidity /= nbHours;
        average_temp /= nbHours;
        average_windSpeed /= nbHours;
        if (nbRainField > 0) {
            average_precipitation /= nbRainField;
        }
        if (average_snowVolume > 0) {
            average_snowVolume /= nbSnowField;
        }

        weatherBulletin.setTemperature(average_temp);
        weatherBulletin.setMinTemperature(min_temp);
        weatherBulletin.setMaxTemperature(max_temp);
        weatherBulletin.setHumidity(average_humidity);
        weatherBulletin.setWindSpeed(average_windSpeed);
        weatherBulletin.setPrecipitation(average_precipitation);
        weatherBulletin.setSnowVolume(average_snowVolume);
        weatherBulletin.setCityName(city_field.getString("name"));
        weatherBulletin.setCountry(city_field.getString("country"));
        weatherBulletin.setCoordonates(coord_field.getDouble("lat"), coord_field.getDouble("lon"));
        String dateFrom = Integer.toString(from.get(Calendar.YEAR)) + "-" + Integer.toString(from.get(Calendar.MONTH))
                + "-" + Integer.toString(from.get(Calendar.DAY_OF_MONTH));
        String dateTo = Integer.toString(to.get(Calendar.YEAR)) + "-" + Integer.toString(to.get(Calendar.MONTH)) + "-"
                + Integer.toString(to.get(Calendar.DAY_OF_MONTH));

        weatherBulletin.setDateFrom(dateFrom);
        weatherBulletin.setDateTo(dateTo);

        return weatherBulletin;
    }

    private void setWeatherForecastTemperature(WeatherBulletin bulletin, String jsonString) {

        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray forecast_temperature = (JSONArray) jsonObject.getJSONArray("list");
        int nbLines = jsonObject.getInt("cnt");

        LinkedHashMap<String, Double> forecastWeather = new LinkedHashMap<String, Double>();

        for (int i = 0; i < nbLines; i++) {
            JSONObject hourlyWeather = forecast_temperature.getJSONObject(i);
            JSONObject main_field = hourlyWeather.getJSONObject("main");

            String date = hourlyWeather.getString("dt_txt");
            double temperature = main_field.getDouble("temp");
            forecastWeather.put(date, temperature);
        }
        bulletin.setForecastWeather(forecastWeather);
    }

    private String setConnection(String urlString) {

        StringBuilder result = new StringBuilder();
        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            // System.out.println(result);

        } catch (IOException e) {
            e.printStackTrace();
            errorMessage("Nous n'arrivons pas à communiquer avec l'api OpenWeather!");
        }

        return result.toString();
    }

    @Override
    public WeatherBulletin getCurrentWeatherBulletin(double longitude, double latitude) throws ParseException {
        String urlString = setUrl("weather", longitude, latitude);

        String response = setConnection(urlString);

        return setWeatherInformations(response);
    }

    @Override
    public WeatherBulletin getWeatherBulletinByDay(double longitude, double latitude, Calendar date)
            throws ParseException {
        if (_errorTrace.getError() != null) {
            return null;
        }
        return getWeatherBulletinByInterval(longitude, latitude, date, date);
    }

    @Override
    public WeatherBulletin getWeatherBulletinByInterval(double longitude, double latitude, Calendar from, Calendar to)
            throws ParseException {
        if (_errorTrace.getError() != null) {
            return null;
        }

        String urlString = setUrl("forecast", longitude, latitude);

        String response = setConnection(urlString);

        return setWeatherInformationsFromTimeInterval(response, from, to);
    }

    @Override
    public ArrayList<WeatherBulletin> getWeatherBulletin(double longitude, double latitude) throws ParseException {
        if (_errorTrace.getError() != null) {
            return null;
        }

        String urlString = setUrl("forecast", longitude, latitude);

        String response = setConnection(urlString);
        return setWeatherInformationsForHourlyForecast(response);

    }

    /**
     * From a forecast in JSON format, fills a list of WeatherBulletin objects.
     * 
     * @param jsonString : json sent by OpenWeather API
     * 
     * @return the list of WeatherBullein objects.
     * @throws ParseException
     */
    public ArrayList<WeatherBulletin> setWeatherInformationsForHourlyForecast(String jsonString) throws ParseException {

        ArrayList<WeatherBulletin> bulletins = new ArrayList<WeatherBulletin>();

        JSONObject jsonObject = new JSONObject(jsonString);

        JSONArray days_array = jsonObject.getJSONArray("list");
        JSONObject city_field = jsonObject.getJSONObject("city");
        JSONObject coord_field = city_field.getJSONObject("coord");

        int nblines = jsonObject.getInt("cnt");
        String previousDay = "";
        double average_temp = 0;
        double average_humidity = 0;
        double min_temp = Double.MAX_VALUE;
        double max_temp = Double.MIN_VALUE;
        double average_windSpeed = 0;
        int nbHours = 0;
        int nbRainField = 0;
        int nbSnowField = 0;
        double average_precipitation = 0;
        double average_snowVolume = 0;
        WeatherBulletin bulletin = null;

        for (int i = 0; i <= nblines; i++) {
            JSONObject hourlyWeather = days_array.getJSONObject(i%nblines);

            // Get the day with the "dt_txt" field.
            String tmp = hourlyWeather.getString("dt_txt");
            String[] dateString = tmp.split(" ");
            String day = dateString[0];
            /*
             * this condition is true if and only if this is the first time we treat this
             * day. Then we reset all variables.
             */
            if (previousDay == "" || !day.equals(previousDay)) {

                /*
                 * it's used to check if we finished the treatement of a day. if it's true, we
                 * update bulletin fields and add it to the list.
                 */
                if (bulletin != null) {

                    average_humidity /= nbHours;
                    average_temp /= nbHours;
                    average_windSpeed /= nbHours;
                    if (nbRainField > 0) {
                        average_precipitation /= nbRainField;
                    }
                    if (average_snowVolume > 0) {
                        average_snowVolume /= nbSnowField;
                    }

                    bulletin.setTemperature(average_temp);
                    System.out.println( String.format(" MIN : Pour la date %s on ajoute la température %s ",previousDay, min_temp ));
                    //System.out.println( String.format(" MAX : Pour la date %s on ajoute la température %s ",day, max_temp ));
                    bulletin.setMinTemperature(min_temp);
                    bulletin.setMaxTemperature(max_temp);
                    bulletin.setHumidity(average_humidity);
                    bulletin.setWindSpeed(average_windSpeed);
                    bulletin.setPrecipitation(average_precipitation);
                    bulletin.setSnowVolume(average_snowVolume);
                    bulletin.setCityName(city_field.getString("name"));
                    bulletin.setCountry(city_field.getString("country"));
                    bulletin.setCoordonates(coord_field.getDouble("lat"), coord_field.getDouble("lon"));
                    bulletin.setDateFrom(previousDay);
                    bulletin.setDateTo(previousDay);
                    bulletins.add(bulletin);
                }
                bulletin = new WeatherBulletin(_errorTrace);

                previousDay = day;
                average_temp = 0;
                average_humidity = 0;
                min_temp = Double.MAX_VALUE;
                max_temp = Double.MIN_VALUE;
                average_windSpeed = 0;
                nbHours = 0;
                nbRainField = 0;
                nbSnowField = 0;
                average_precipitation = 0;
                average_snowVolume = 0;
            }
            nbHours += 1;

            JSONObject wind_field = hourlyWeather.getJSONObject("wind");
            JSONObject main_field = hourlyWeather.getJSONObject("main");

            average_temp += main_field.getDouble("temp");
            double tmp_min_temp = main_field.getDouble("temp_min");
            double tmp_max_temp = main_field.getDouble("temp_max");
            if (min_temp > tmp_min_temp) {
                System.out.println( String.format(" MIN : Pour la date %s la temperature %s remplace %s ",day, tmp_min_temp, min_temp ));
                min_temp = tmp_min_temp;
            }

            if (max_temp < tmp_max_temp) {
                //System.out.println( String.format(" MAX : Pour la date %s la temperature %s remplace %s ",day, tmp_max_temp, max_temp ));
                max_temp = tmp_max_temp;
            }

            average_humidity += main_field.getDouble("humidity");
            average_windSpeed += wind_field.getDouble("speed");
            if (hourlyWeather.has("rain")) {
                JSONObject rain_field = hourlyWeather.getJSONObject("rain");
                average_precipitation += rain_field.getDouble("3h");
                nbRainField += 1;
            }

            if (hourlyWeather.has("snow")) {
                JSONObject snow_field = hourlyWeather.getJSONObject("snow");
                average_snowVolume += snow_field.getDouble("3h");
                nbSnowField += 1;
            }
        }
        /*
         * we need to store hourly forecasts, and we decide to store in the first
         * weatherBulletin object.
         */
        bulletin = getCurrentWeatherBulletin(coord_field.getDouble("lon"), coord_field.getDouble("lat"));

        setWeatherForecastTemperature(bulletin, jsonString);

        bulletins.set(0, bulletin);
        for (WeatherBulletin b : bulletins) {
            System.out.println(b.toString());
        }
        return bulletins;
    }

}
