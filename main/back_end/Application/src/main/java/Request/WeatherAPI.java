package Request;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

import Exceptions.ErrorTrace;
import Parser.*;


/**
 * This abstract describes the functions that a weather API must perform.
 * we can add a new API by creating a class extending this abstract.
 * 
 * This API will be used to get forecasts.
 * 
 */


public abstract class WeatherAPI implements Cloneable{
    ErrorTrace _errorTrace;
    /**
     * From longitude and latitude, get the current weather.
     * @param longitude
     * @param latitude
     * @return the bulletin associated to the weather.
     * @throws ParseException 
     */
    public abstract WeatherBulletin getCurrentWeatherBulletin (double longitude,double latitude) throws ParseException;

    /**
     * From longitude and latitude, get the average weather at the date specified.
     * @param longitude
     * @param latitude
     * @param date
     * @return the bulletin associated to the weather.
     * @throws ParseException 
     */
    public abstract WeatherBulletin getWeatherBulletinByDay(double longitude,double latitude,Calendar date) throws ParseException;

    /**
     * From longitude and latitude, get the average weather between two dates specified.
     * @param longitude
     * @param latitude
     * @param from
     * @param to
     * @return the bulletin associated to the weather.
     * @throws ParseException 
     */
    public abstract WeatherBulletin getWeatherBulletinByInterval(double longitude,double latitude,Calendar from,Calendar to) throws ParseException;


    /**
     * From latitude and longitude, we get the average weather for each day, 
     * except for the first one where we take the current weather
     * by default, we're looking at the next 4 days.
     * @param longitude
     * @param latitude
     * @param from
     * @param to
     * @return the list of bulletins associated.
     * @throws ParseException 
     */
    public abstract ArrayList<WeatherBulletin> getWeatherBulletin(double longitude,double latitude) throws ParseException;
    
    
    public Object clone(){
        Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException cnse) {
			cnse.printStackTrace(System.err);
		}
		return o;
    }

    public void errorMessage(String message){
        System.out.println(message);
        _errorTrace.setError(message);
    }

    public void warningMessage(String message){
        System.out.println(message);
        _errorTrace.addWarning(message);
    }

    /**
     * Set the errorTrace object to the generator.
     * See ErrorTrace description.
     * @param errorTrace
     */
    public void setErrorTrace(ErrorTrace errorTrace){
        _errorTrace=errorTrace;
    }

}