package Server;


import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.text.ParseException;
import java.util.ArrayList;

import org.json.JSONObject;
import TextGenerator.RunTextGenerator;
import Exceptions.ErrorTrace;

import Parser.WeatherJsonProcessing;
import Parser.WeatherBulletin;
import Request.WeatherAPI;

/**
 * ClientHandler is the class assiociated with each connection. it will
 * communicate with the connected smartphone .
 * 
 * @see listenMsg(Socket s);
 * @see sendMsg(Socket s,JSONObject data);
 */

public class ClientHandler extends Thread {
	private double _latitude;
	private double _longitude;
	private Socket _socket;
	private WeatherAPI _weatherAPI;
	private RunTextGenerator _runTextGenerator;

	/**
	 * Each client has an ID.
	 * 
	 */

	/**
	 * This field is used to check the client inactivity
	 * @see void run()
	 */
	private int _deconnectionTime;

	/**
	 * _errorTrace field stock errors and warnings met during the phrase generation
	 * process. We pass it to weatherAPI and runTextGenerator objects.
	 * 
	 * @see ClientHandler(Socket socket, WeatherAPI weatherAPI, RunTextGenerator
	 *      runTextGenerator)
	 * 
	 */
	private ErrorTrace _errorTrace;

	public ClientHandler(Socket socket, WeatherAPI weatherAPI, RunTextGenerator newRunTextGenerator) {
		_errorTrace = new ErrorTrace();
		_socket = socket;
		_weatherAPI = weatherAPI;
		_runTextGenerator = newRunTextGenerator;
		_weatherAPI.setErrorTrace(_errorTrace);
		_runTextGenerator.setErrorTrace(_errorTrace);
	}

	public void run() {
			listenMsg(_socket);
			try {
				
				sendMsg(_socket, generateJsonResponseForOneDay());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				System.out.println("Déconnexion du client");
				_socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
	}
	
    private void sendMsg(Socket s,JSONObject data) {

		try {
			OutputStream out = s.getOutputStream();
			ObjectOutputStream obj = new ObjectOutputStream(out);
			obj.writeObject(data.toString());
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	private boolean listenMsg(Socket s) {
		String response = null;
		try {
			InputStream in = s.getInputStream();
			ObjectInputStream obj = new ObjectInputStream(in);
			response= (String) obj.readObject();           
			setCoordonnates(response);
			return true;

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}


	/**
	 * From the json, this function will set longitude and latitudes fields.
	 * 
	 * @param json : the json get from the application.
	 */
	private void setCoordonnates(String json) {
		JSONObject jsonObject = new JSONObject(json);
		this._latitude = jsonObject.getDouble("latitude");
		this._longitude = jsonObject.getDouble("longitude");
    }


	/**
	 * This function will call the weather API linked to the client to get a list of forecasts.
	 * 
	 * @return The list of forecasts.
	 * @throws ParseException 
	 */
	private ArrayList<WeatherBulletin> getWeatherBulletins() throws ParseException{
		if(_errorTrace.getError()!=null){
            return null;
		}
		ArrayList<WeatherBulletin> bulletins = _weatherAPI.getWeatherBulletin(_longitude,_latitude);
		return bulletins;
	}


	/**
	 * This function will generate a sentence for each day.
	 * 
	 * @return the Json that we will send to the application.
	 * @throws ParseException
	 */	
	private JSONObject generateJsonResponseForEachDay() throws ParseException {
		ArrayList<WeatherBulletin> bulletins;
		bulletins = getWeatherBulletins();
		ArrayList<String> sentences = _runTextGenerator.getGeneratorResponse(bulletins);
		WeatherJsonProcessing json = new WeatherJsonProcessing();
		JSONObject forecastBulletin = json.jsonBuilder(sentences, bulletins, _errorTrace);
		return forecastBulletin;
	}


	/**
	 * This function will generate a sentence just for the first day.
	 * It will go faster than  generateJsonResponseForEachDay().
	 * 
	 * @return the Json that we will send to the application.
	 * @throws ParseException
	 */	
	private JSONObject generateJsonResponseForOneDay() throws ParseException {
		ArrayList<WeatherBulletin> bulletins;
		bulletins = getWeatherBulletins();
		String sentence = _runTextGenerator.getGeneratorResponse(bulletins.get(0));
		WeatherJsonProcessing json = new WeatherJsonProcessing();
		JSONObject forecastBulletin = json.jsonBuilder(sentence, bulletins, _errorTrace);
		System.out.println(forecastBulletin);
		return forecastBulletin;
	}


	
}