package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import TextGenerator.RunTextGenerator;
import Request.WeatherAPI;

/**
 * The ServerTcp class manages new connexions and associates to each connexion a
 * Client class.
 * 
 * @throws IOException
 */

public class ServerTCP {
	private WeatherAPI weatherAPI;
	private RunTextGenerator runTextGenerator;
	private ServerSocket server;

	/**
	 * ServerTCP constructor.
	 * 
	 * @param weatherAPI
	 * @param runTextGenerator
	 */
	public ServerTCP(WeatherAPI weatherAPI, RunTextGenerator runTextGenerator) {

		this.weatherAPI = weatherAPI;
		this.runTextGenerator = runTextGenerator;
	}

	public void startServer() throws IOException {

		System.out.println("Hello Server");
		this.server = new ServerSocket(4400);

		while (true) {

			Socket s = server.accept();
			System.out.println("Client connected");

			WeatherAPI newWeatherAPI = (WeatherAPI) weatherAPI.clone();
			RunTextGenerator newRunTextGenerator = (RunTextGenerator) runTextGenerator.clone();
			
			ClientHandler clt = new ClientHandler(s,newWeatherAPI,newRunTextGenerator);
			clt.start();
			
				
		}
		
	}
	
}
