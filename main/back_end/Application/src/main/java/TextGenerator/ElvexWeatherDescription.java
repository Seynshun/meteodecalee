package TextGenerator;

import java.util.HashMap;

import Request.MeteoStatsRequest;
import Enums.*;
import Exceptions.ErrorTrace;
import Parser.WeatherBulletin;


/**
 * This class is used to analyze weather data collected from the WeatherBulletin class 
 * to associate each weather feature with an intensity,
 * which will then be used by Elvex.
 * 
 * For example:
 * If we see heavy rains, we will associate to the rainIntensity variable, IntensityEnum.LOT.
 * @see setWeatherStates(WeatherBulletin bulletin)
 * 
 */

public class ElvexWeatherDescription {
  //http://pluiesextremes.meteo.fr/france-metropole/Intensite-de-precipitations.html
  final int LOW_LEVEL_PRECIPITATION = 1;
  final int NORMAL_LEVEL_PRECIPITATION = 4;
  final int HIGH_LEVEL_PRECIPITATION = 8;

  //https://www.meteo-laille.fr/L_beaufort.php
  final int LOW_WIND_SPEED = 0;
  final int NORMAL_WIND_SPEED = 19;
  final int HIGH_WIND_SPEED = 49;


  final int LOW_LEVEL_SNOW = 10;
  final int NORMAL_LEVEL_SNOW = 100;
  final int HIGH_LEVEL_SNOW = 500;


  private IntensityEnum rainIntensity = IntensityEnum.NONE;
  private IntensityEnum windIntensity= IntensityEnum.NONE;
  private IntensityEnum coldIntensity= IntensityEnum.NONE;
  private IntensityEnum snowIntensity= IntensityEnum.NONE;
  private IntensityEnum hotIntensity= IntensityEnum.NONE;

  private ErrorTrace _errorTrace;
  public ElvexWeatherDescription(WeatherBulletin bulletin,ErrorTrace errorTrace){
    _errorTrace = errorTrace;
    setWeatherStates(bulletin);
  }
  

  public IntensityEnum getRainIntensity(){
    return rainIntensity;
  }

  public IntensityEnum getWindIntensity(){
    return windIntensity;
  }

  public IntensityEnum getColdIntensity(){
    return coldIntensity;
  }

  public IntensityEnum getSnowIntensity(){
    return snowIntensity;
  }

  public IntensityEnum getHotIntensity(){
    return hotIntensity;
  }



  
  private void setWeatherStates(WeatherBulletin bulletin){
    if(_errorTrace.getError()!=null){
      return ;
    }
    double currentTemp = bulletin.getTemperature();
    MeteoStatsRequest meteoStatsRequest = new MeteoStatsRequest();
    meteoStatsRequest.setErrorTrace(_errorTrace);
    String[] date = bulletin.getDateTo().split("-");
    HashMap<String,Double>normals =  meteoStatsRequest.getNormalsFromMonth(Integer.parseInt(date[1]), bulletin.getLatitude(), bulletin.getLongitude());
    
    //set RAIN variable: 
    if(bulletin.getPrecipitation()>LOW_LEVEL_PRECIPITATION){
      this.rainIntensity= IntensityEnum.FEW;
    }
    if(bulletin.getPrecipitation()>NORMAL_LEVEL_PRECIPITATION){
      this.rainIntensity= IntensityEnum.NORMAL;
    }
    if(bulletin.getPrecipitation()>HIGH_LEVEL_PRECIPITATION){
      this.rainIntensity= IntensityEnum.LOT;
    }
    

    if ( normals != null){

    
    //set HOT variable:
      if(normals.get("temperature") + (normals.get("temperature")*45)/100  < currentTemp ){
        this.hotIntensity= IntensityEnum.FEW;
        if( normals.get("temperature") + (normals.get("temperature")*45)/100 <currentTemp){
          this.hotIntensity= IntensityEnum.LOT;
        }
      }



      //set COLD variable:
      if(normals.get("temperature") - (normals.get("temperature")*45)/100  > currentTemp ){
        this.coldIntensity= IntensityEnum.FEW;
        if( normals.get("temperature") - (normals.get("temperature")*45)/100  > currentTemp){
          this.coldIntensity= IntensityEnum.LOT;
        }
      }

    }

    //set Snow variable:
    if(bulletin.getSnowVolume()>LOW_LEVEL_SNOW){
      this.snowIntensity= IntensityEnum.FEW;
    }
    if(bulletin.getSnowVolume()>NORMAL_LEVEL_SNOW){
      this.snowIntensity= IntensityEnum.NORMAL;
    }
    if(bulletin.getSnowVolume()>HIGH_LEVEL_SNOW){
      this.snowIntensity= IntensityEnum.LOT;
    }
    
    
    
    //set WIND variable: 
    if(bulletin.getWindSpeed() * 3.6> LOW_WIND_SPEED ){
      this.windIntensity= IntensityEnum.FEW;
    }
    if(bulletin.getWindSpeed() * 3.6> NORMAL_WIND_SPEED){
      this.windIntensity= IntensityEnum.NORMAL;
    }
    if(bulletin.getWindSpeed() * 3.6> HIGH_WIND_SPEED ){
      this.windIntensity= IntensityEnum.LOT;
    }

  }


  
}








