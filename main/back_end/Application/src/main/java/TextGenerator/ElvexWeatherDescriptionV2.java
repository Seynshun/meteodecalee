package TextGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import Request.MeteoStatsRequest;
import Exceptions.ErrorTrace;
import Parser.WeatherBulletin;

/**
 * This class is used to analyze weather data collected from the WeatherBulletin
 * class to associate each weather feature with an intensity, which will then be
 * used by Elvex.
 * 
 * For example: If we see heavy rains, we will associate to the rainIntensity
 * variable, IntensityEnum.LOT.
 * 
 * @see setWeatherStates(WeatherBulletin bulletin)
 * 
 */

public class ElvexWeatherDescriptionV2 {
    // http://pluiesextremes.meteo.fr/france-metropole/Intensite-de-precipitations.html
    final int LOW_LEVEL_PRECIPITATION = 1;
    final int NORMAL_LEVEL_PRECIPITATION = 4;
    final int HIGH_LEVEL_PRECIPITATION = 8;

    // https://www.meteo-laille.fr/L_beaufort.php
    final int LOW_WIND_SPEED = 0;
    final int NORMAL_WIND_SPEED = 19;
    final int HIGH_WIND_SPEED = 49;

    final int LOW_LEVEL_SNOW = 10;
    final int NORMAL_LEVEL_SNOW = 100;
    final int HIGH_LEVEL_SNOW = 500;

    private JSONObject weatherDescription;

    private ErrorTrace _errorTrace;
    public Map<Integer, String> _months = new HashMap<>();
    public Map<Integer, String> _days = new HashMap<>();

    public ElvexWeatherDescriptionV2(WeatherBulletin bulletin, ErrorTrace errorTrace) {
        _errorTrace = errorTrace;
        setMonths();
        setDays();
        weatherDescription = new JSONObject();
        JSONObject main = new JSONObject();
        main.put("main_pred","heat");
        main.put("degree","few");
        JSONObject seconde= new JSONObject();
        seconde.put("pred","");
        seconde.put("gender","male");
        weatherDescription.put("i", main);
        weatherDescription.put("ii", seconde);
        weatherDescription.put("tense", "near_future");
        weatherDescription.put("language_register", "slang");

        setWeatherStates(bulletin);

    }

    public JSONObject getWeatherDescription() {
        return weatherDescription;
    }

    private void setWeatherStates(WeatherBulletin bulletin) {
        if (_errorTrace.getError() != null) {
            return;
        }
        double currentTemp = bulletin.getTemperature();
        MeteoStatsRequest meteoStatsRequest = new MeteoStatsRequest();
        meteoStatsRequest.setErrorTrace(_errorTrace);
        String[] date = bulletin.getDateTo().split("-");
        HashMap<String, Double> normals = meteoStatsRequest.getNormalsFromMonth(Integer.parseInt(date[1]),
                bulletin.getLatitude(), bulletin.getLongitude());
        //System.out.println(normals.get("temperature"));
        if (normals != null) {
            try {
                setDateValue(bulletin);
            
            } catch (ParseException e) {
                e.printStackTrace();
            }
        
                    
	        // set Snow variable:
	        if (checkSnowVariable(bulletin.getSnowVolume())) {
	            System.out.println("SNOW");  
	            return;
	        }  
	        
	        // set RAIN variable:
	        if (checkRainVariable(bulletin.getPrecipitation())) {
	            System.out.println("RAIN");
	            return;
	        }
	
	        // set COLD variable:
	        if (checkColdVariable(normals.get("temperature"), currentTemp)) {
	            System.out.println("COLD");
	            return;
	        }
	
	        // set HOT variable:
	        if (checkHotVariable(normals.get("temperature"), currentTemp)) {
	            System.out.println("HOT");
	            return;
	        }
	
	        // setWindVariable
	        if (checkWindVariable(bulletin.getWindSpeed())) {
	            System.out.println("WIN");
	            return;
	        }
        }
    // set default
    setDefaultVariable();
    }

    private boolean checkRainVariable(double precipitation) {
        if (precipitation <= LOW_LEVEL_PRECIPITATION) {
            return false;
        }
        JSONObject main = new JSONObject();
        JSONObject seconde = new JSONObject();

        main.put("weather","rain");
        seconde.put("pred","happy");
        if (precipitation > LOW_LEVEL_PRECIPITATION) {
            main.put("degree", "few");
        }
        if (precipitation > NORMAL_LEVEL_PRECIPITATION) {
            main.put("degree", "regular");
        }
        if (precipitation > HIGH_LEVEL_PRECIPITATION) {
            main.put("degree", "a_lot");
        }
        weatherDescription.put("i", main);
        weatherDescription.put("ii", seconde);
        weatherDescription.put("type", "cause");
        

        return true;

    }

    private boolean checkSnowVariable(double snowVolume) {
        if (snowVolume <= 0.0) {
            return false;
        }

        JSONObject main = new JSONObject();
        JSONObject seconde = new JSONObject();
        
        main.put("weather","snow");
        seconde.put("pred","unhappy");
        if (snowVolume > LOW_LEVEL_SNOW) {
            main.put("degree", "few");
        }
        if (snowVolume > NORMAL_LEVEL_SNOW) {
            main.put("degree", "regular");
        }
        if (snowVolume > HIGH_LEVEL_SNOW) {
            main.put("degree", "a_lot");
        }
        weatherDescription.put("language_register","null");
        weatherDescription.put("i", main);
        weatherDescription.put("ii", seconde);

        return true;
    }

    private boolean checkWindVariable(double windSpeed) {

        if (windSpeed <= 0.0) {
            return false;
        }
        JSONObject main = new JSONObject();
        JSONObject seconde = new JSONObject();

        main.put("weather","wind");
        seconde.put("pred","unhappy");

        if (windSpeed * 3.6 > HIGH_WIND_SPEED) {
            main.put("degree", "a_lot");
        } else {
            return false;
        }

        weatherDescription.put("i", main);
        weatherDescription.put("ii", seconde);
        weatherDescription.put("type", "cause");

        return true;

    }

    private boolean checkHotVariable(double normalsTemperature, double currentTemp) {

        JSONObject main = new JSONObject();
        JSONObject seconde = new JSONObject();

        main.put("weather","heat");
        seconde.put("pred","happy");

        if (normalsTemperature + (normalsTemperature * 45) / 100 < currentTemp) {
            main.put("degree", "few");
            if (normalsTemperature + (normalsTemperature * 45) / 100 < currentTemp) {
                main.put("degree", "a_lot");
            }
        }

        else {
            return false;
        }

        weatherDescription.put("i", main);
        weatherDescription.put("ii", seconde);
        weatherDescription.put("type", "cause");

        return true;
    }

    private boolean checkColdVariable(double normalsTemperature, double currentTemp) {

        JSONObject main = new JSONObject();
        JSONObject seconde = new JSONObject();

        main.put("weather","cold");
        seconde.put("pred","unhappy");

        if (normalsTemperature - (normalsTemperature * 45) / 100 > currentTemp) {
            main.put("degree", "few");
            if (normalsTemperature - (normalsTemperature * 45) / 100 > currentTemp) {
                main.put("degree", "a_lot");
            }
        } else {
            return false;
        }
        weatherDescription.put("i", main);
        weatherDescription.put("ii", seconde);
        weatherDescription.put("type", "cause");

        return true;
    }

    private boolean setDefaultVariable() {

        JSONObject main = new JSONObject();
        main.put("weather","heat");
        main.put("degree","few");
        JSONObject seconde= new JSONObject();
        seconde.put("pred","happy");
        weatherDescription.put("i", main);
        weatherDescription.put("ii", seconde);
        weatherDescription.put("tense", "near_future");
        weatherDescription.put("language_register", "slang");
        weatherDescription.put("cause","type");
        return true;

    }

    private void setDateValue(WeatherBulletin bulletin) throws ParseException {
        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        Date day= sdf.parse(bulletin.getDateFrom());
        Date currDate = sdf.parse(currentDate);
        
        long difference = day.getTime() - currDate.getTime();
        float daysBetween = (difference / (1000*60*60*24));

        if(daysBetween==0){
            weatherDescription.put("time", "today");
            Calendar cal1 = Calendar.getInstance();
            if(cal1.get(Calendar.HOUR_OF_DAY) > 15){
                weatherDescription.put("time", "tonight");
            }
            return;
        }
        if(daysBetween==1){
            weatherDescription.put("time", "tomorrow");
            return;
        }


        Calendar c = Calendar.getInstance();
        c.setTime(day);
        String dayOfWeek = _days.get(c.get(Calendar.DAY_OF_WEEK));
        String month = _months.get(c.get(Calendar.MONTH));
        
        
        weatherDescription.put("time", " \"ce "+ dayOfWeek +" "+c.get(Calendar.DAY_OF_MONTH)+" "+ month+ "\"");
        return;
    }
    private void setMonths(){
        _months.put(0, "Janvier");
        _months.put(1, "Fevrier");
        _months.put(2, "Mars");
        _months.put(3, "Avril");
        _months.put(4, "Mai");
        _months.put(5, "Juin");
        _months.put(6, "Juillet");
        _months.put(7, "Aout");
        _months.put(8, "Septembre");
        _months.put(9, "Novembre");
        _months.put(10, "Octobre");
        _months.put(11, "Décembre");
    }

    private void setDays(){
        _days.put(1, "Dimanche");
        _days.put(2, "Lundi");
        _days.put(3, "Mardi");
        _days.put(4, "Mercredi");
        _days.put(5, "Jeudi");
        _days.put(6, "Vendredi");
        _days.put(7, "Samedi");
    }


}
