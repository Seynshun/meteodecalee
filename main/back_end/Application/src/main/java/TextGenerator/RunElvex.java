package TextGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import Parser.WeatherBulletin;

/**
 * This class is used to call the Elvex tool from the directory stocked in _elvexPath variable.
 * 
 * @see getGeneratorResponse(WeatherBulletin bulletin)
 */
public class RunElvex extends RunTextGenerator {
    private String _elvexPath= "../../../CLEMENT";
    private String generateConcept(WeatherBulletin bulletin){
        if(_errorTrace.getError()!=null){
            return null;
        }
        System.out.println("Generate Concept");

        ElvexWeatherDescription wState = new ElvexWeatherDescription(bulletin,_errorTrace);
        
        switch(wState.getRainIntensity()){
            case FEW:
                return "text [PRED:_pleuvoir, lexical_function:un_peu, language_register:slang]";
                
            case NORMAL:
                return "text [PRED:_pleuvoir, language_register:slang]";
                
            case LOT:
                return "text [PRED:_pleuvoir, lexical_function:beaucoup, language_register:slang]";
                
            case NONE:
                break;
        }
        switch(wState.getColdIntensity()){
            case FEW:
            return "text [PRED:_faire_froid, lexical_function:un_peu]";
                
            case NORMAL:
                return "text [PRED:cause, i: [PRED:_faire_froid], ii: [PRED:_être_mécontent, i:[PRED:__pro, person:two, number:sg], modV:<[PRED:_encore]>], tense:near_future, language_register:slang, modSType:time, modS:<[FORM: \"Aujourd'hui\" ]>]";
                
            case LOT:
                return "text [PRED:_faire_froid, lexical_function:very_much]";
                
            case NONE:
                break;  
        }
        switch(wState.getHotIntensity()){
            case FEW:
                return "text [PRED:_faire_chaud, language_register:slang, lexical_function:un_peu]";
                
            case NORMAL:
                return "text [PRED:_faire_chaud, language_register:slang]";
            case LOT:
                break;   
            case NONE:
                break;      
        }

        switch(wState.getSnowIntensity()){
            case FEW:
                return "text [PRED:_neiger, language_register:slang]";
                
            case NORMAL:
                return "text [PRED:_neiger, language_register:slang]";
            case LOT:
                break;   
            case NONE:
                break;      
        }
        
        System.out.println("Neutre");
        return "text [PRED:cause, i: [PRED:_faire_beau],ii: [PRED:_être_content, i:[PRED:__pro, person:two, number:sg]],tense:near_future,language_register:slang,modSType:time, modS:<[FORM:\"Aujourd'hui\"]>]";

    }
    


    private void makeProgram(){
        if(_errorTrace.getError()!=null){
            return ;
        }
        ProcessBuilder pb1 = new ProcessBuilder("make", "-s");
        ProcessBuilder pb2 = new ProcessBuilder("make", "-s");
        try {
            pb1.directory(new File(this._elvexPath));
            Process p1 = pb1.start();
            String response = readProcessOutput(p1);
            System.out.println(response);
        }catch(Exception ex) {
            errorMessage("Nous ne pouvons joindre Elvex");
            ex.printStackTrace();
        }

        try {
            pb2.directory(new File(this._elvexPath+"/data/lefff"));
            Process p2 = pb2.start();
            String response = readProcessOutput(p2);
            System.out.println(response);
        }catch(Exception ex) {
            errorMessage("Nous ne pouvons joindre Elvex");
            ex.printStackTrace();
        }
    }

     
    /**
     * Reads the response from the command. Please note that this works only
     * if the process returns immediately.
     * @param p
     * @return
     * @throws Exception 
     */
    private String readProcessOutput(Process p) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String response = "";
        String line;
        while ((line = reader.readLine()) != null) {
            response += line+"\n";
        }
        reader.close();
        return response;
    }

    private HashMap<Integer,String> GetElvexOutput(Process p) throws Exception{
        if(_errorTrace.getError()!=null){
            return null;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        HashMap<Integer,String> response = new HashMap<Integer,String>() ;
        String line;
        int numberOfValidsPhrases=0;
        while ((line = reader.readLine()) != null) {
            
            line = line.replace("   "," ");
            line = line.replace("   "," ");
            if(!line.contains("_")){
                response.put(numberOfValidsPhrases, line);
                numberOfValidsPhrases+=1;
            }
        }
        reader.close();
        if(numberOfValidsPhrases==0){
            errorMessage("Elvex n'a généré aucune phrase.");
        }
        return response;
    }


    @Override
    public String getGeneratorResponse(WeatherBulletin bulletin) {
        
        if(_errorTrace.getError()!=null){
            return null;
        }
        //makeProgram();
        String p = generateConcept(bulletin);


        List<String> commandAndArgs = new ArrayList<String>();
        String command = "../src/elvex";
        String[] arguments = {"-compactLexiconDirectory","lefff","-compactLexiconFile","lefff","-grammarFile","pdp-meteo.grammar","-lexiconFile","pdp-meteo.local-lexicon"};
        String[] remain =  {"| sed 's/  */ /g' | ../src/elvexpostedition | tr '@' '\n' |sed -e 's/^ *//' |tr '@' '\n'"};
        HashMap<Integer,String> response = new HashMap<Integer,String>() ;
        
        commandAndArgs.add(command);

        for(String each : arguments)
            commandAndArgs.add(each);

        commandAndArgs.add(p);

        for(String each : remain)
            commandAndArgs.add(each);

        ProcessBuilder pb = new ProcessBuilder(commandAndArgs);
        try {
            pb.directory(new File(this._elvexPath+"/data"));
            Process process = pb.start();
            response = GetElvexOutput(process);
            
        }catch(Exception ex) {
            errorMessage("Nous ne pouvons joindre Elvex");
            ex.printStackTrace();
            return null;
        }
 

        Random rand= new Random();
        return response.get(rand.nextInt(response.size())); 
    }

    
    public ArrayList<String> getGeneratorResponse(ArrayList<WeatherBulletin> bulletins){
        if(_errorTrace.getError()!=null){
            return null;
        }
        ArrayList<String> responses = new ArrayList<String>();
        for(WeatherBulletin bulletin: bulletins){
            String response=getGeneratorResponse(bulletin);
            if(response ==null){
                return null;
            }
            responses.add(response);

        }

        if(responses.size()!= bulletins.size()){
            errorMessage("Nous n'avons pas pu générer toutes les phrases");
        }

        return responses;
    }

    private String sentenceBuilder(){
        //simple


        //complexe


        

        return null;
    }
}