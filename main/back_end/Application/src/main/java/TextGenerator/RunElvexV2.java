package TextGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import Parser.WeatherBulletin;

/**
 * This class is used to call the Elvex tool from the directory stocked in _elvexPath variable.
 * 
 * @see getGeneratorResponse(WeatherBulletin bulletin)
 */
public class RunElvexV2 extends RunTextGenerator {  
    private String _elvexPath = "../../../CLEMENT";
    private String dictionnaryPath = "jsonFiles/elvexDictionnary.json";

    private JSONObject dictionnary;
    private JSONObject defaultDescription;


    public RunElvexV2(){
        try {
            String dictContent = Files.readString(Paths.get(dictionnaryPath));

            dictionnary = new JSONObject(dictContent);
            defaultDescription= new JSONObject();
        } catch (IOException | JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }


    private String generateConcept(WeatherBulletin bulletin){
        
        System.out.println("Generate Concept");
        ElvexWeatherDescriptionV2 wState = new ElvexWeatherDescriptionV2(bulletin, _errorTrace);
        
        if(_errorTrace.getError()!=null){
            System.out.println(_errorTrace.getError());
            return null;
        }
        defaultDescription = wState.getWeatherDescription();
        System.out.println(defaultDescription);
        
        JSONObject main_field =  defaultDescription.getJSONObject("i");
        String main_pred = dictionnary.getJSONObject("weather").getString(main_field.getString("weather"));
        String degree = dictionnary.getJSONObject("degree").getString(main_field.getString("degree"));
        String time = "";
        String ii_pred="";
        String type ="";
        String tense = dictionnary.getJSONObject("tense").getString(defaultDescription.getString("tense"));
        String language_register =defaultDescription.getString("language_register") =="null"? "": dictionnary.getJSONObject("language_register").getString(defaultDescription.getString("language_register"));
        String weatherDescription="";
        String iiString="";

        if( dictionnary.getJSONObject("time").has(defaultDescription.getString("time"))){
            time = dictionnary.getJSONObject("time").getString(defaultDescription.getString("time"));
        }
        else{
            time =defaultDescription.getString("time");  
        }

        if(defaultDescription.has("type")){
            weatherDescription = String.format( "[PRED:%s, degree:%s ]",main_pred,degree);
            if(defaultDescription.has("ii")){
                type=defaultDescription.getString("type");
                if(type.equals("cause")){
                    
                    JSONObject mood_field = defaultDescription.getJSONObject("ii");
                    ii_pred = dictionnary.getJSONObject("mood").getString(mood_field.getString("pred"));
                    iiString=String.format("[PRED:%s, i:[PRED:__pro, person:two, number:sg]] ", ii_pred);
                }
                String tmpResult = "text [PRED:%s, i: %s, ii: %s, tense:%s, language_register:%s, modSType:time, modS:<[FORM: %s ]>]";
                String result = String.format(tmpResult,type,weatherDescription,iiString, tense, language_register,time);
                return result;
            }
            
        }
        if(language_register==""){
            weatherDescription = "text [PRED:%s, degree:%s,tense:%s,modSType:time, modS:<[FORM: %s ]> ]";
            weatherDescription = String.format(weatherDescription,main_pred,degree,tense,time);
        }
        else{
            weatherDescription = "text [PRED:%s, degree:%s,tense:%s,language_register:%s, modSType:time, modS:<[FORM: %s ]> ]";
            weatherDescription = String.format(weatherDescription,main_pred,degree,tense,language_register,time);
        }
        return weatherDescription;
       
    }
    


    private void makeProgram(){
        if(_errorTrace.getError()!=null){
            return ;
        }
        ProcessBuilder pb1 = new ProcessBuilder("make", "-s");
        ProcessBuilder pb2 = new ProcessBuilder("make", "-s");
        try {
            pb1.directory(new File(this._elvexPath));
            Process p1 = pb1.start();
            String response = readProcessOutput(p1);
            System.out.println(response);
        }catch(Exception ex) {
            errorMessage("Nous ne pouvons joindre Elvex");
            ex.printStackTrace();
        }

        try {
            pb2.directory(new File(this._elvexPath+"/data/lefff"));
            Process p2 = pb2.start();
            String response = readProcessOutput(p2);
            System.out.println(response);
        }catch(Exception ex) {
            errorMessage("Nous ne pouvons joindre Elvex");
            ex.printStackTrace();
        }
    }

     
    /**
     * Reads the response from the command. Please note that this works only
     * if the process returns immediately.
     * @param p
     * @return
     * @throws Exception 
     */
    private String readProcessOutput(Process p) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String response = "";
        String line;
        while ((line = reader.readLine()) != null) {
            response += line+"\n";
        }
        reader.close();
        return response;
    }

    private HashMap<Integer,String> GetElvexOutput(Process p) throws Exception{
        if(_errorTrace.getError()!=null){
            return null;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        HashMap<Integer,String> response = new HashMap<Integer,String>() ;
        String line;
        int numberOfValidsPhrases=0;
        while ((line = reader.readLine()) != null) {
            
            line = line.replace("   "," ");
            line = line.replace("   "," ");
            if(!line.contains("_")){
                response.put(numberOfValidsPhrases, line);
                numberOfValidsPhrases+=1;
            }
        }
        reader.close();
        if(numberOfValidsPhrases==0){
            errorMessage("Elvex n'a généré aucune phrase.");
        }
        return response;
    }


    @Override
    public String getGeneratorResponse(WeatherBulletin bulletin) {
        
        
        //makeProgram();
        String p = generateConcept(bulletin);
        if(_errorTrace.getError()!=null){
            return null;
        }
        System.out.println(p);

        List<String> commandAndArgs = new ArrayList<String>();
        String command = "../src/elvex";
        String[] arguments = {"-compactLexiconDirectory","lefff","-compactLexiconFile","lefff","-grammarFile","pdp-meteo.grammar","-lexiconFile","pdp-meteo.local-lexicon"};
        String[] remain =  {"| sed 's/  */ /g' | ../src/elvexpostedition | tr '@' '\n' |sed -e 's/^ *//' |tr '@' '\n'"};
        HashMap<Integer,String> response = new HashMap<Integer,String>() ;
        
        commandAndArgs.add(command);

        for(String each : arguments)
            commandAndArgs.add(each);

        commandAndArgs.add(p);

        for(String each : remain)
            commandAndArgs.add(each);

        ProcessBuilder pb = new ProcessBuilder(commandAndArgs);
        try {
            pb.directory(new File(this._elvexPath+"/data"));
            Process process = pb.start();
            response = GetElvexOutput(process);
            
        }catch(Exception ex) {
            errorMessage("Nous ne pouvons joindre Elvex");
            ex.printStackTrace();
            return null;
        }
        Random rand= new Random();
        String generatedSentence = response.get(rand.nextInt(response.size()));
        System.out.println(generatedSentence);
        return generatedSentence;

    }

    
    public ArrayList<String> getGeneratorResponse(ArrayList<WeatherBulletin> bulletins){
        
        ArrayList<String> responses = new ArrayList<String>();
        for(WeatherBulletin bulletin: bulletins){
            String response=getGeneratorResponse(bulletin);
            if(_errorTrace.getError()!=null){
                return null;
            }
            responses.add(response);

        }

        if(responses.size()!= bulletins.size()){
            errorMessage("Nous n'avons pas pu générer toutes les phrases");
        }

        return responses;
    }
}