package TextGenerator;

import java.util.ArrayList;

import Exceptions.ErrorTrace;
import Parser.WeatherBulletin;

/**
 * This interface describes the functions that a text generator must perform
 * 
 */


public abstract class RunTextGenerator implements Cloneable{
    ErrorTrace _errorTrace;
    /**
     * Creates a sentence from the bulletin received . 
     * @param bulletin
     * @return the sentence describing the weather
     */
    public abstract String getGeneratorResponse(WeatherBulletin bulletin);

    public abstract ArrayList<String> getGeneratorResponse(ArrayList<WeatherBulletin> bulletins);
    public Object clone(){
        Object o = null;
		try {
			// On récupère l'instance à renvoyer par l'appel de la 
			// méthode super.clone()
			o = super.clone();
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous implémentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
    }



    /**
     * Set the errorTrace object to the generator.
     * See ErrorTrace description.
     * @param errorTrace
     */
    public void setErrorTrace(ErrorTrace errorTrace){
        _errorTrace=errorTrace;
    }

    public ErrorTrace getErrorTrace(){
        return this._errorTrace;
    }

    public void errorMessage(String message){
        System.out.println(message);
        _errorTrace.setError(message);
    }

    public void warningMessage(String message){
        System.out.println(message);
        _errorTrace.addWarning(message);
    }

}
