package Exceptions;

import static org.junit.Assert.*;

import org.junit.Test;

public class ErrorTraceTest {

	@Test
	public final void testErrorTrace() {
		ErrorTrace errorTrace = new ErrorTrace();
		assertNull("Should be empty",  errorTrace.getError());
		assertEquals("Should be empty", true, errorTrace.getWarnings().isEmpty());
	}

	@Test
	public final void testAddWarningNotNull() {
		ErrorTrace errorTrace = new ErrorTrace();
		errorTrace.addWarning("error1");
		errorTrace.addWarning("error2");
		assertEquals("Should not be empty", false, errorTrace.getWarnings().isEmpty());	
		assertEquals("Should be equals : error1", "error1", errorTrace.getWarnings().get(0));	
		assertEquals("Should be equals : error2", "error2", errorTrace.getWarnings().get(1));	
	}

	@Test
	public final void testAddWarningNull() {
		ErrorTrace errorTrace = new ErrorTrace();
		errorTrace.addWarning("error1");
		errorTrace.addWarning("error1");
		assertEquals("Should not be empty", false, errorTrace.getWarnings().isEmpty());	
		assertEquals("Should be equals : error1", "error1", errorTrace.getWarnings().get(0));	
		assertEquals("Should not be twice error1", 1, errorTrace.getWarnings().size());	
	}
	
	@Test
	public final void testSetError() {
		ErrorTrace errorTrace = new ErrorTrace();
		errorTrace.setError("error1");
		assertEquals("Should be equals : error", "error1", errorTrace.getError());		
	}

	@Test
	public final void testClearWarnings() {
		ErrorTrace errorTrace = new ErrorTrace();
		errorTrace.addWarning("error1");
		assertEquals("Should not be empty : warningsList", false, errorTrace.getWarnings().isEmpty());	
		errorTrace.clearWarnings();
		assertEquals("Should be empty : warningsList", true, errorTrace.getWarnings().isEmpty());	
	}

	@Test
	public final void testClone() {
		ErrorTrace errorTrace = new ErrorTrace();
		errorTrace.addWarning("error1");
		errorTrace.addWarning("error2");
		errorTrace.setError("ErrorAgain");	
		assertEquals("Should be equals : errorTrace objects", errorTrace.toString(), errorTrace.clone().toString());
	}

}
