package Parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Calendar;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import Exceptions.ErrorTrace;

public class WeatherBulletinTest {
private static final double DELTA = 1e-15;

	@Test
	public final void testSetDateFromYesterdayDate() throws ParseException {
		final Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -2);
	    String yesterdayDate= Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
                + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setDateFrom(yesterdayDate);
		assertNotNull("Should throw error : the day is not current day", errorTrace.getError());
	}

	@Test
	public final void testSetDateToWrongDay() throws ParseException {
		String wrongDay= "1862-20-6524";
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setDateTo(wrongDay);
		assertNotNull("Should throw error : the day is not a correct day", errorTrace.getError());
	}
	
	@Test
	public final void testSetDateToNormalDay() throws ParseException {
		final Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, +4);
	    String normalDay = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
        + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setDateTo(normalDay);
		assertNull("Should not throw error : the day is a correct day", errorTrace.getError());
	}

	@Test
	public final void testSetDateFromNormalDay() throws ParseException {
		final Calendar cal = Calendar.getInstance();
	    String normalDay = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
        + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setDateFrom(normalDay);
		assertNull("Should not throw error : the day is a correct day", errorTrace.getError());
	}
	
	@Test
	public final void testSetDateFromWrongDate() throws ParseException {
	    String wrongDay= "1862-20-6524";
	    
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setDateFrom(wrongDay);
		assertNotNull("Should throw error : the day is not a correct day", errorTrace.getError());
	}
	
	@Test
	public final void testSetTemperatureHighTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setTemperature(80);
		assertNotNull("Should throw error : too high temperature", errorTrace.getError());
	}
	
	@Test
	public final void testSetTemperatureLowTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setTemperature(-70);
		assertNotNull("Should throw error : too low temperature", errorTrace.getError());
	}
	
	@Test
	public final void testSetTemperatureNormalTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setTemperature(0);
		assertNull("Should not throw error : normal temperature", errorTrace.getError());
	}

	@Test
	public final void testSetMinTemperatureHighTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setMinTemperature(80);
		assertNotNull("Should throw error : too high temperature", errorTrace.getError());
	}
	
	@Test
	public final void testSetMinTemperatureLowTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setMinTemperature(-70);
		assertNotNull("Should throw error : too low temperature", errorTrace.getError());
	}
	
	@Test
	public final void testSetMinTemperatureNormalTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setMinTemperature(0);
		assertNull("Should not throw error : normal temperature", errorTrace.getError());
	}
	
	@Test
	public final void testSetMaxTemperatureHighTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setMaxTemperature(80);
		assertNotNull("Should throw error : too high temperature", errorTrace.getError());
	}
	
	@Test
	public final void testSetMaxTemperatureLowTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setMaxTemperature(-70);
		assertNotNull("Should throw error : too low temperature", errorTrace.getError());
	}
	
	@Test
	public final void testSetMaxTemperatureNormalTemperature() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setMaxTemperature(0);
		assertNull("Should not throw error : normal temperature", errorTrace.getError());
	}

	@Test
	public final void testSetHumidityLowHumidity() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setHumidity(-1);
		assertNotNull("Should throw error : too low humidity", errorTrace.getError());	
	}

	@Test
	public final void testSetHumidityHighHumidity() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setHumidity(102);
		assertNotNull("Should throw error : too high humidity", errorTrace.getError());	
	}
	
	@Test
	public final void testSetHumidityNormalHumidity() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setHumidity(0);
		assertNull("Should not throw error : normal humidity", errorTrace.getError());	
	}
	
	@Test
	public final void testSetWindSpeedHighWindSpeed() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setWindSpeed(210);
		assertNotNull("Should throw error : too high wind speed", errorTrace.getError());	
	}
	
	@Test
	public final void testSetWindSpeedLowWindSpeed() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setWindSpeed(-1);
		assertNotNull("Should throw error : too low wind speed", errorTrace.getError());	
	}
	
	@Test
	public final void testSetWindSpeedNormalWindSpeed() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setWindSpeed(0);
		assertNull("Should not throw error : normal wind speed", errorTrace.getError());	
	}
	
	@Test
	public final void testSetCoordonatesLowLatitude() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCoordonates(-100,0);
		assertNotNull("Should throw error : too low latitude", errorTrace.getError());		
	}
	
	@Test	
	public final void testSetCoordonatesLowLongitude() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCoordonates(-0,-185);
		assertNotNull("Should throw error : too low longitude", errorTrace.getError());		
	}
	
	@Test	
	public final void testSetCoordonatesHighLatitude() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCoordonates(100,0);
		assertNotNull("Should throw error : too high latitude", errorTrace.getError());		
	}
	
	@Test	
	public final void testSetCoordonatesHighLongitude() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCoordonates(0,185);
		assertNotNull("Should throw error : too high longitude", errorTrace.getError());		
	}
	
	@Test	
	public final void testSetCoordonatesNormalCoordinates() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCoordonates(0,0);
		assertNull("Should not throw error : normal longitude and latitude", errorTrace.getError());		
	}
	
	@Test
	public final void testSetPrecipitationNormalPrecipitation() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setPrecipitation(0);
		assertNull("Should not throw error : normal rain precipitation", errorTrace.getError());		
	}

	@Test
	public final void testSetPrecipitationHighPrecipitation() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setPrecipitation(50);
		assertNotNull("Should throw error : too high precipitation", errorTrace.getError());		
	}
	
	@Test
	public final void testSetPrecipitationLowPrecipitation() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setPrecipitation(-0.1);
		assertNotNull("Should throw error : too low precipitation", errorTrace.getError());		
	}

	@Test
	public final void testSetCityNameNull() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCityName(null);
		assertNotNull("Should throw error : city name null", errorTrace.getError());	
	}

	@Test
	public final void testSetCityNameExistingCity() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCityName("Bordeaux");
		assertNull("Should not throw error : existing city name", errorTrace.getError());
	}
	
	@Test
	public final void testSetCountryNullCountry() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCountry(null);
		assertNotNull("Should throw error : not valid country", errorTrace.getError());		
	}
	
	@Test
	public final void testSetCountryNormalCountry() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setCountry("France");
		assertNull("Should not throw : valid country", errorTrace.getError());		
	}

	@Test
	public final void testSetSnowVolumeLowSnowVolume() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setSnowVolume(-0.5);
		assertNotNull("Should throw error : too low snow volume", errorTrace.getError());	
	}
	
	@Test
	public final void testSetSnowVolumeHighSnowVolume() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setSnowVolume(1000);
		assertNotNull("Should throw error : too high snow volume", errorTrace.getError());	
	}
	
	@Test
	public final void testSetSnowVolumeNormalSnowVolume() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setSnowVolume(0);
		assertNull("Should not throw error : normal snow volume", errorTrace.getError());	
	}
	
	@Test
	public final void testToStringFromTodayTo5DaysAfter() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		Calendar cal = Calendar.getInstance();
	    String todayDay = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
        + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
	    cal.add(Calendar.DATE, +5);
	    String after5Days = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
        + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
        String city_name = "Bordeaux";
        String temperature = "10.0";
        String minTemperature = "-5.0";
        String maxTemperature = "20.5";
        String windSpeed = "1.0";
        String humidity = "8.5" ;
        String precipitation = "0.5" ;
        String snowVolume = "0.0" ;
        
        String dateString = "Du "+ todayDay+ " au "+after5Days+" \n";
        String city_nameString = "A: "+ city_name+"\n";
        String temperatureString = "Il fait: "+ temperature + " C\n";
        String minTemperatureString = "min: "+minTemperature+ " C\n";
        String maxTemperatureString = "max: "+maxTemperature+ " C\n";
        String windSpeedString = "un vent a "+ windSpeed + " m/s\n";
        String humidityString = "l'humidite:  "+ humidity+"%\n" ;
        String precipitationString = "pluie:  "+ precipitation+"mm\n" ;
        String snowVolumeString = "neige:  "+ snowVolume+"mm\n" ;
		weatherBulletin.setCityName("Bordeaux");
		weatherBulletin.setDateFrom(todayDay);
		weatherBulletin.setDateTo(after5Days);
		weatherBulletin.setTemperature(Double.valueOf(temperature));
		weatherBulletin.setMinTemperature(Double.valueOf(minTemperature));
		weatherBulletin.setMaxTemperature(Double.valueOf(maxTemperature));
		weatherBulletin.setWindSpeed(Double.valueOf(windSpeed));
		weatherBulletin.setHumidity(Double.valueOf(humidity));
		weatherBulletin.setPrecipitation(Double.valueOf(precipitation));
		weatherBulletin.setSnowVolume(Double.valueOf(snowVolume));
		String expectedString = city_nameString+dateString+temperatureString+minTemperatureString+maxTemperatureString+windSpeedString+humidityString+precipitationString+snowVolumeString;
		assertEquals("Both weatherBulletin.toString() should be the same : normal snow volume", expectedString, weatherBulletin.toString());	
	}
	
	@Test
	public final void testGetDateFrom() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		Calendar cal = Calendar.getInstance();
	    String todayDay = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
        + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		weatherBulletin.setDateFrom(todayDay);
		String expectedDate = todayDay;
		assertEquals("Should be equals : dateFrom", expectedDate, weatherBulletin.getDateFrom());	
	}
	
	@Test
	public final void testGetDateTo() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		Calendar cal = Calendar.getInstance();
	    String todayDay = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
        + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		weatherBulletin.setDateTo(todayDay);
		String expectedDate = todayDay;
		assertEquals("Should be equals : dateTo", expectedDate, weatherBulletin.getDateTo());	
	}
	
	@Test
	public final void testGetTemperature() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 20;
		weatherBulletin.setTemperature(expectedDouble);
		assertEquals("Should be equals : temperature", expectedDouble, weatherBulletin.getTemperature(), DELTA);	
	}
	
	@Test
	public final void testGetMinTemperature() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 20;
		weatherBulletin.setMinTemperature(expectedDouble);
		assertEquals("Should be equals : min temperature", expectedDouble, weatherBulletin.getMinTemperature(), DELTA);	
	}
	
	@Test
	public final void testGetMaxTemperature() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 20;
		weatherBulletin.setMaxTemperature(expectedDouble);
		assertEquals("Should be equals : min temperature", expectedDouble, weatherBulletin.getMaxTemperature(), DELTA);	
	}
	
	@Test
	public final void testGetHumidity() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 20;
		weatherBulletin.setHumidity(expectedDouble);
		assertEquals("Should be equals : humidity", expectedDouble, weatherBulletin.getHumidity(), DELTA);	
	}
	
	@Test
	public final void testGetWindSpeed() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 20;
		weatherBulletin.setWindSpeed(expectedDouble);
		assertEquals("Should be equals : humidity", expectedDouble, weatherBulletin.getWindSpeed(), DELTA);	
	}
	
	@Test
	public final void testGetPrecipitation() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 20;
		weatherBulletin.setPrecipitation(expectedDouble);
		assertEquals("Should be equals : Precipitation", expectedDouble, weatherBulletin.getPrecipitation(), DELTA);	
	}
	
	@Test
	public final void testGetErrorTraceNoError() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		assertEquals("Should be equals : errorTrace", "Voici les warnings rencontres:\n", errorTrace.toString());	
	}
	
	@Test
	public final void testGetErrorTraceWithError() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		errorTrace.setError("Erreur");
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		String msg = "Expected <" + weatherBulletin.getErrorTrace() + "> to be unequal to <" + null +">";
		assertFalse(msg, weatherBulletin.getErrorTrace().equals(null));
	}
	
	@Test
	public final void testGetCityName() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		String expectedName = "Bordeaux";
		weatherBulletin.setCityName(expectedName);
		assertEquals("Should be equals : city name", expectedName, weatherBulletin.getCityName());	
	}
	
	@Test
	public final void testToJSON() throws ParseException {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
        JSONObject expectedResult = new JSONObject();
		Calendar cal = Calendar.getInstance();
	    String dateFrom = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
        + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
        String cityname = "Bordeaux";
        double temperature = 10.0;
        double minTemperature = -5.0;
        double maxTemperature = 20.5;
        double longitude = 50;
        double latitude = 50;
		weatherBulletin.setCityName(cityname);
		weatherBulletin.setDateFrom(dateFrom);
		weatherBulletin.setCoordonates(latitude, longitude);
		weatherBulletin.setTemperature(temperature);
		weatherBulletin.setMinTemperature(minTemperature);
		weatherBulletin.setMaxTemperature(maxTemperature);
        expectedResult.put("city", cityname);
        expectedResult.put("longitude", longitude);
        expectedResult.put("latitude", latitude);
        expectedResult.put("temperature", temperature);
        expectedResult.put("temperature_max", maxTemperature);
        expectedResult.put("temperature_min", minTemperature);
        expectedResult.put("date",dateFrom);
		assertEquals("Should be equals : same weatherBulletin", expectedResult.toString(), weatherBulletin.toJson().toString());

	}

	
	@Test
	public final void testSetForecastWeather() {
		ErrorTrace errorTrace = new ErrorTrace();
		LinkedHashMap<String,Double> forecastWeather = new LinkedHashMap<String,Double>();
		forecastWeather.put("Pluie", 10.0);
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setForecastWeather(forecastWeather);
		assertNull("Should not throw error : normal forecastWeather", errorTrace.getError());
    }
	
	@Test
	public final void testGetForecastWeather() {
		ErrorTrace errorTrace = new ErrorTrace();
		LinkedHashMap<String,Double> forecastWeather = new LinkedHashMap<String,Double>();
		forecastWeather.put("4-1-2020", 10.0);
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		weatherBulletin.setForecastWeather(forecastWeather);
        JSONArray list = new JSONArray();
        for (String i : forecastWeather.keySet()) {
            JSONObject forecastTemperature = new JSONObject();
            forecastTemperature.put("date",i);
            forecastTemperature.put("temperature",forecastWeather.get(i));
            list.put(forecastTemperature);
        }
        JSONArray array = weatherBulletin.getForecastArray();
        JSONObject data = array.getJSONObject(0);
		assertTrue(data.has("date"));
		Object dateObject = data.get("date");
		assertTrue(dateObject.equals("4-1-2020"));
		assertTrue(data.has("temperature"));
		Object temperatureObject = data.get("temperature");
		assertTrue(temperatureObject.equals(10.0));
    }
	
	@Test
	public final void testGetForecastWeatherNull() {
		ErrorTrace errorTrace = new ErrorTrace();
		LinkedHashMap<String,Double> forecastWeather = new LinkedHashMap<String,Double>();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		assertNull("Should be null : getForecastArray", weatherBulletin.getForecastArray());
    }
	
	@Test
	public final void testGetCountry() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		assertNull("Should be null : no country name", weatherBulletin.getCountry());		
	}
	
	@Test
	public final void testGetLatitude() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 20;
		weatherBulletin.setCoordonates(expectedDouble, 10);
		assertEquals("Should be equals :latitude", expectedDouble, weatherBulletin.getLatitude(), DELTA);			
	}
	
	@Test
	public final void testGetLongitude() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 20;
		weatherBulletin.setCoordonates(10, expectedDouble);
		assertEquals("Should be equals :longitude", expectedDouble, weatherBulletin.getLongitude(), DELTA);				
	}
	
	@Test
	public final void testSnowVolume() {
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin weatherBulletin = new WeatherBulletin(errorTrace);
		double expectedDouble = 1;
		weatherBulletin.setSnowVolume(expectedDouble);
		assertEquals("Should be equals : min temperature", expectedDouble, weatherBulletin.getSnowVolume(), DELTA);	
	}
}
