package Parser;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Before;
import org.junit.Test;

import Exceptions.ErrorTrace;
import Request.OpenWeatherMapRequest;

public class WeatherJsonProcessingTest {
    JSONParser parser = new JSONParser();

	@Test
	public final void testJsonBuilder() throws ParseException, FileNotFoundException, IOException, org.json.simple.parser.ParseException {
		ArrayList<WeatherBulletin> bulletins= new ArrayList<WeatherBulletin>();
		ArrayList<String> sentences = new ArrayList<String>();
		
		//create WeatherBulletin0 and WeatherBulletin1 with no error
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin bulletin0 =  new WeatherBulletin(errorTrace);
		bulletin0.setDateFrom("2020-3-29");
		bulletin0.setCityName("Talence");
		bulletin0.setCoordonates(44.8, -0.6);
		bulletin0.setTemperature(8.79);
		bulletin0.setMaxTemperature(10);
		bulletin0.setMinTemperature(8);
		//add sentence according to WeatherBulletin
		String sentence0 = new String("  Ce soir tu vas etre content , on va avoir la raie en sauce .  ");
		sentences.add(sentence0);
		//add forecastWeather
		LinkedHashMap<String,Double> forecastWeather0 = new LinkedHashMap<String,Double>();
		forecastWeather0.put("2020-03-29 18:00:00", 8.29);
		forecastWeather0.put("2020-03-29 21:00:00", 6.01);
		forecastWeather0.put("2020-03-30 00: 00: 00", 3.65);
		forecastWeather0.put("2020-03-30 03: 00: 00", 2.8);
		forecastWeather0.put("2020-03-30 06: 00: 00", 2.42);
		forecastWeather0.put("2020-03-30 09: 00: 00", 3.76);
		forecastWeather0.put("2020-03-30 12: 00: 00", 4.86);
		forecastWeather0.put("2020-03-30 15: 00: 00", 5.12);
		forecastWeather0.put("2020-03-30 18: 00: 00", 4.29);
		forecastWeather0.put("2020-03-30 21: 00: 00", 4.03);
		forecastWeather0.put("2020-03-31 00:00:00", 3.97);
		forecastWeather0.put("2020-03-31 03:00:00", 3.08);
		forecastWeather0.put("2020-03-31 06:00:00", 1.77);
		forecastWeather0.put("2020-03-31 09:00:00", 6.04);
		forecastWeather0.put("2020-03-31 12: 00: 00", 10.62);
		forecastWeather0.put("2020-03-31 15: 00: 00", 12.25);
		forecastWeather0.put("2020-03-31 18: 00: 00", 10.27);
		forecastWeather0.put("2020-03-31 21: 00: 00", 7.84);
		forecastWeather0.put("2020-04-01 00: 00: 00", 6.67);
		forecastWeather0.put("2020-04-01 03: 00: 00", 6.3);
		forecastWeather0.put("2020-04-01 06: 00: 00", 6.78);
		forecastWeather0.put("2020-04-01 09: 00: 00", 12.8);
		forecastWeather0.put("2020-04-01 12: 00: 00", 16.65);
		forecastWeather0.put("2020-04-01 15: 00: 00", 17.02);
		forecastWeather0.put("2020-04-01 18: 00: 00", 13.25);
		forecastWeather0.put("2020-04-01 21: 00: 00", 9.12);
		forecastWeather0.put("2020-04-02 00:00:00", 7.66);
		forecastWeather0.put("2020-04-02 03:00:00", 5.93);
		forecastWeather0.put("2020-04-02 06:00:00", 5.04);
		forecastWeather0.put("2020-04-02 09: 00: 00", 10.77);
		forecastWeather0.put("2020-04-02 12: 00: 00", 15.11);
		forecastWeather0.put("2020-04-02 15:00:00", 15.14);
		forecastWeather0.put("2020-04-02 18:00:00", 11.39);
		forecastWeather0.put("2020-04-02 21: 00: 00", 7.6);
		forecastWeather0.put("2020-04-03 00: 00: 00", 5.43);
		forecastWeather0.put("2020-04-03 03: 00: 00", 3.68);
		forecastWeather0.put("2020-04-03 06:00:00", 3.28);
		forecastWeather0.put("2020-04-03 09:00:00", 9.33);
		forecastWeather0.put("2020-04-03 12:00:00", 12.04);
		forecastWeather0.put("2020-04-03 15:00:00", 12.47);
		
		bulletin0.setForecastWeather(forecastWeather0);
		//add WeatherBulletin to the list
		bulletins.add(bulletin0);
		
		
		//create WeatherforecastWeather2 with no error
		ErrorTrace errorTrace2 = new ErrorTrace();
		WeatherBulletin bulletin2 =  new WeatherBulletin(errorTrace2);
		bulletin2.setDateFrom("2020-3-30");
		bulletin2.setCityName("Talence");
		bulletin2.setCoordonates(44.8, -0.6);
		bulletin2.setTemperature(7.1499999999999995);
		bulletin2.setMaxTemperature(8.29);
		bulletin2.setMinTemperature(5.01);
		//add sentence according to WeatherBulletin
		String sentence2 = new String("  Demain tu vas te frotterles mains , on va avoir les rillettes sous les bras .  ");
		sentences.add(sentence2);
		//add WeatherBulletin to the list
		bulletins.add(bulletin2);
		
		//create Weatherbulletin3 with no error
		ErrorTrace errorTrace3 = new ErrorTrace();
		WeatherBulletin bulletin3 =  new WeatherBulletin(errorTrace3);
		bulletin3.setDateFrom("2020-3-31");
		bulletin3.setCityName("Talence");
		bulletin3.setCoordonates(44.8, -0.6);
		bulletin3.setTemperature(3.86625);
		bulletin3.setMaxTemperature(5.12);
		bulletin3.setMinTemperature(2.42);
		//add sentence according to WeatherBulletin
		String sentence3 = new String("  ce Mardi 31 Mars tu vas faire la gueule parce que on va se geler les roubignoles .  ");
		sentences.add(sentence3);
		//add WeatherBulletin to the list
		bulletins.add(bulletin3);
		
		//create Weatherbulletin4 with no error
		ErrorTrace errorTrace4 = new ErrorTrace();
		WeatherBulletin bulletin4 =  new WeatherBulletin(errorTrace4);
		bulletin4.setDateFrom("2020-4-01");
		bulletin4.setCityName("Talence");
		bulletin4.setCoordonates(44.8, -0.6);
		bulletin4.setTemperature(6.98);
		bulletin4.setMaxTemperature(12.25);
		bulletin4.setMinTemperature(1.77);
		//add sentence according to WeatherBulletin
		String sentence4 = new String("  ce Mercredi 1 Avril tu vas faire la tronche parce que on va se geler les roubignoles .  ");
		sentences.add(sentence4);
		//add WeatherBulletin to the list
		bulletins.add(bulletin4);
		
		//create Weatherbulletin5 with no error
		ErrorTrace errorTrace5 = new ErrorTrace();
		WeatherBulletin bulletin5 =  new WeatherBulletin(errorTrace5);
		bulletin5.setDateFrom("2020-4-02");
		bulletin5.setCityName("Talence");
		bulletin5.setCoordonates(44.8, -0.6);
		bulletin5.setTemperature(11.07375);
		bulletin5.setMaxTemperature(17.02);
		bulletin5.setMinTemperature(6.3);
		//add sentence according to WeatherBulletin
		String sentence5 = new String("  ce Jeudi 2 Avril tu vas peter joie , il va faire plus chaud que sous l'aiselle d'un tondeur .  ");
		sentences.add(sentence5);
		//add WeatherBulletin to the list
		bulletins.add(bulletin5);
		
		//create Weatherbulletin6 with no error
		ErrorTrace errorTrace6 = new ErrorTrace();
		WeatherBulletin bulletin6 =  new WeatherBulletin(errorTrace6);
		bulletin6.setDateFrom("2020-4-03");
		bulletin6.setCityName("Talence");
		bulletin6.setCoordonates(44.8, -0.6);
		bulletin6.setTemperature(9.829999999999998);
		bulletin6.setMaxTemperature(15.14);
		bulletin6.setMinTemperature(5.04);
		//add sentence according to WeatherBulletin
		String sentence6 = new String("  ce Vendredi 3 Avril tu vas etre a les anges parce que on va avoir les olives qui baignent dans l'huile .  ");
		sentences.add(sentence6);
		//add WeatherBulletin to the list
		bulletins.add(bulletin6);

		
		Object content = parser.parse(new FileReader("jsonFiles/test.json"));

        JSONObject jsonObject = new JSONObject(content);
		String stringExpected = content.toString();
        
		ErrorTrace errorTraceBuild = new ErrorTrace();
		JSONObject jsonResult = new JSONObject();
		WeatherJsonProcessing wjs = new WeatherJsonProcessing();
		jsonResult = wjs.jsonBuilder(sentences, bulletins,errorTraceBuild);
		
		assertEquals("Should be equals", stringExpected, jsonResult.toString());
	
	}
	
	@Test
	public final void testJsonBuilderWithWarningsAndErrors() throws ParseException, FileNotFoundException, IOException, org.json.simple.parser.ParseException {
		ArrayList<WeatherBulletin> bulletins= new ArrayList<WeatherBulletin>();
		ArrayList<String> sentences = new ArrayList<String>();
		
		//create WeatherBulletin0 and WeatherBulletin1 with no error
		ErrorTrace errorTrace = new ErrorTrace();
		WeatherBulletin bulletin0 =  new WeatherBulletin(errorTrace);
		bulletin0.setDateFrom("2020-3-29");
		bulletin0.setCityName("Talence");
		bulletin0.setCoordonates(44.8, -0.6);
		bulletin0.setTemperature(8.79);
		bulletin0.setMaxTemperature(10);
		bulletin0.setMinTemperature(8);
		//add sentence according to WeatherBulletin
		String sentence0 = new String("  Ce soir tu vas etre content , on va avoir la raie en sauce .  ");
		sentences.add(sentence0);
		//add forecastWeather
		LinkedHashMap<String,Double> forecastWeather0 = new LinkedHashMap<String,Double>();
		forecastWeather0.put("2020-03-29 18:00:00", 8.29);
		forecastWeather0.put("2020-03-29 21:00:00", 6.01);
		forecastWeather0.put("2020-03-30 00: 00: 00", 3.65);
		forecastWeather0.put("2020-03-30 03: 00: 00", 2.8);
		forecastWeather0.put("2020-03-30 06: 00: 00", 2.42);
		forecastWeather0.put("2020-03-30 09: 00: 00", 3.76);
		forecastWeather0.put("2020-03-30 12: 00: 00", 4.86);
		forecastWeather0.put("2020-03-30 15: 00: 00", 5.12);
		forecastWeather0.put("2020-03-30 18: 00: 00", 4.29);
		forecastWeather0.put("2020-03-30 21: 00: 00", 4.03);
		forecastWeather0.put("2020-03-31 00:00:00", 3.97);
		forecastWeather0.put("2020-03-31 03:00:00", 3.08);
		forecastWeather0.put("2020-03-31 06:00:00", 1.77);
		forecastWeather0.put("2020-03-31 09:00:00", 6.04);
		forecastWeather0.put("2020-03-31 12: 00: 00", 10.62);
		forecastWeather0.put("2020-03-31 15: 00: 00", 12.25);
		forecastWeather0.put("2020-03-31 18: 00: 00", 10.27);
		forecastWeather0.put("2020-03-31 21: 00: 00", 7.84);
		forecastWeather0.put("2020-04-01 00: 00: 00", 6.67);
		forecastWeather0.put("2020-04-01 03: 00: 00", 6.3);
		forecastWeather0.put("2020-04-01 06: 00: 00", 6.78);
		forecastWeather0.put("2020-04-01 09: 00: 00", 12.8);
		forecastWeather0.put("2020-04-01 12: 00: 00", 16.65);
		forecastWeather0.put("2020-04-01 15: 00: 00", 17.02);
		forecastWeather0.put("2020-04-01 18: 00: 00", 13.25);
		forecastWeather0.put("2020-04-01 21: 00: 00", 9.12);
		forecastWeather0.put("2020-04-02 00:00:00", 7.66);
		forecastWeather0.put("2020-04-02 03:00:00", 5.93);
		forecastWeather0.put("2020-04-02 06:00:00", 5.04);
		forecastWeather0.put("2020-04-02 09: 00: 00", 10.77);
		forecastWeather0.put("2020-04-02 12: 00: 00", 15.11);
		forecastWeather0.put("2020-04-02 15:00:00", 15.14);
		forecastWeather0.put("2020-04-02 18:00:00", 11.39);
		forecastWeather0.put("2020-04-02 21: 00: 00", 7.6);
		forecastWeather0.put("2020-04-03 00: 00: 00", 5.43);
		forecastWeather0.put("2020-04-03 03: 00: 00", 3.68);
		forecastWeather0.put("2020-04-03 06:00:00", 3.28);
		forecastWeather0.put("2020-04-03 09:00:00", 9.33);
		forecastWeather0.put("2020-04-03 12:00:00", 12.04);
		forecastWeather0.put("2020-04-03 15:00:00", 12.47);
		
		bulletin0.setForecastWeather(forecastWeather0);
		//add WeatherBulletin to the list
		bulletins.add(bulletin0);
		
		
		//create WeatherforecastWeather2 with no error
		ErrorTrace errorTrace2 = new ErrorTrace();
		WeatherBulletin bulletin2 =  new WeatherBulletin(errorTrace2);
		bulletin2.setDateFrom("2020-3-30");
		bulletin2.setCityName("Talence");
		bulletin2.setCoordonates(44.8, -0.6);
		bulletin2.setTemperature(7.1499999999999995);
		bulletin2.setMaxTemperature(8.29);
		bulletin2.setMinTemperature(5.01);
		//add sentence according to WeatherBulletin
		String sentence2 = new String("  Demain tu vas te frotterles mains , on va avoir les rillettes sous les bras .  ");
		sentences.add(sentence2);
		//add WeatherBulletin to the list
		bulletins.add(bulletin2);
		
		//create Weatherbulletin3 with no error
		ErrorTrace errorTrace3 = new ErrorTrace();
		WeatherBulletin bulletin3 =  new WeatherBulletin(errorTrace3);
		bulletin3.setDateFrom("2020-3-31");
		bulletin3.setCityName("Talence");
		bulletin3.setCoordonates(44.8, -0.6);
		bulletin3.setTemperature(3.86625);
		bulletin3.setMaxTemperature(5.12);
		bulletin3.setMinTemperature(2.42);
		//add sentence according to WeatherBulletin
		String sentence3 = new String("  ce Mardi 31 Mars tu vas faire la gueule parce que on va se geler les roubignoles .  ");
		sentences.add(sentence3);
		//add WeatherBulletin to the list
		bulletins.add(bulletin3);
		
		//create Weatherbulletin4 with no error
		ErrorTrace errorTrace4 = new ErrorTrace();
		WeatherBulletin bulletin4 =  new WeatherBulletin(errorTrace4);
		bulletin4.setDateFrom("2020-4-01");
		bulletin4.setCityName("Talence");
		bulletin4.setCoordonates(44.8, -0.6);
		bulletin4.setTemperature(6.98);
		bulletin4.setMaxTemperature(12.25);
		bulletin4.setMinTemperature(1.77);
		//add sentence according to WeatherBulletin
		String sentence4 = new String("  ce Mercredi 1 Avril tu vas faire la tronche parce que on va se geler les roubignoles .  ");
		sentences.add(sentence4);
		//add WeatherBulletin to the list
		bulletins.add(bulletin4);
		
		//create Weatherbulletin5 with no error
		ErrorTrace errorTrace5 = new ErrorTrace();
		WeatherBulletin bulletin5 =  new WeatherBulletin(errorTrace5);
		bulletin5.setDateFrom("2020-4-02");
		bulletin5.setCityName("Talence");
		bulletin5.setCoordonates(44.8, -0.6);
		bulletin5.setTemperature(11.07375);
		bulletin5.setMaxTemperature(17.02);
		bulletin5.setMinTemperature(6.3);
		//add sentence according to WeatherBulletin
		String sentence5 = new String("  ce Jeudi 2 Avril tu vas peter joie , il va faire plus chaud que sous l'aiselle d'un tondeur .  ");
		sentences.add(sentence5);
		//add WeatherBulletin to the list
		bulletins.add(bulletin5);
		
		//create Weatherbulletin6 with no error
		ErrorTrace errorTrace6 = new ErrorTrace();
		WeatherBulletin bulletin6 =  new WeatherBulletin(errorTrace6);
		bulletin6.setDateFrom("2020-4-03");
		bulletin6.setCityName("Talence");
		bulletin6.setCoordonates(44.8, -0.6);
		bulletin6.setTemperature(9.829999999999998);
		bulletin6.setMaxTemperature(15.14);
		bulletin6.setMinTemperature(5.04);
		//add sentence according to WeatherBulletin
		String sentence6 = new String("  ce Vendredi 3 Avril tu vas etre a les anges parce que on va avoir les olives qui baignent dans l'huile .  ");
		sentences.add(sentence6);
		//add WeatherBulletin to the list
		bulletins.add(bulletin6);

		
		Object content = parser.parse(new FileReader("jsonFiles/testJsonBuilderWithErrors.json"));

        JSONObject jsonObject = new JSONObject(content);
		String stringExpected = content.toString();
        
		ErrorTrace errorTraceBuild = new ErrorTrace();
		errorTraceBuild.addWarning("Warning_1");
		errorTraceBuild.addWarning("Warning_2");
		errorTraceBuild.setError("Error_1");

		JSONObject jsonResult = new JSONObject();
		WeatherJsonProcessing wjs = new WeatherJsonProcessing();
		jsonResult = wjs.jsonBuilder(sentences, bulletins,errorTraceBuild);
		assertEquals("Should be equals", stringExpected, jsonResult.toString());
	
	}
}
