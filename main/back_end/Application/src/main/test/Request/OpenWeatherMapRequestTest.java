package Request;

import static org.junit.Assert.*;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Before;
import org.junit.Test;

import Exceptions.ErrorTrace;
import Parser.WeatherBulletin;


public class OpenWeatherMapRequestTest {
    JSONParser parser = new JSONParser();
    private static final double DELTA = 1e-15;


	@Before
	public void setUp() throws Exception {

	}

	@Test
	public final void setWeatherInformationsForHourlyForecastNormalJSON() {
	    try {
            OpenWeatherMapRequest openWeatherMapRequest = new OpenWeatherMapRequest();
            ArrayList<WeatherBulletin> weatherBulletin= new ArrayList<WeatherBulletin>();
			 //Retrieving example.json content and parse it to String
			Object content = parser.parse(new FileReader("jsonFiles/talenceHourlyForecast.json"));
            JSONObject jsonObject = (JSONObject) content;
			String object = jsonObject.toString();
			            
            weatherBulletin = openWeatherMapRequest.setWeatherInformationsForHourlyForecast(object);
            for(WeatherBulletin b: weatherBulletin){
        		assertNull("Should not throw error : normal bulletin", b.getErrorTrace());
            }
	    } 
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	@Test
	public final void setWeatherInformationsNormalJSONWith3hRainAndSnow() {
	    try {
            OpenWeatherMapRequest openWeatherMapRequest = new OpenWeatherMapRequest();
            ErrorTrace errorTrace = new ErrorTrace();
            WeatherBulletin weatherBulletin= new WeatherBulletin(errorTrace);
			 //Retrieving example.json content and parse it to String
			Object content = parser.parse(new FileReader("jsonFiles/testSetWeatherInformations3h.json"));
            JSONObject jsonObject = (JSONObject) content;
			String object = jsonObject.toString();
	        Calendar cal = Calendar.getInstance();
	        String day = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
	                + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
	        
            weatherBulletin = openWeatherMapRequest.setWeatherInformations(object);            
            assertTrue("Should be equals : city name", weatherBulletin.getCityName().equals("Talence"));
            assertTrue("Should be equals : dateFrom", weatherBulletin.getDateFrom().equals(day));
            assertTrue("Should be equals : dateTo", weatherBulletin.getDateTo().equals(day));


            assertEquals("Should be equals :min temperature", 11.0, weatherBulletin.getMinTemperature(), DELTA);			

    		assertEquals("Should be equals : max temperature", weatherBulletin.getMaxTemperature(), 13.0, DELTA);
    		assertEquals("Should be equals : wind speed", weatherBulletin.getWindSpeed(), 7.7, DELTA);
    		assertEquals("Should be equals : humidity", weatherBulletin.getHumidity(), 62.0, DELTA);
            assertEquals("Should be equals : precipitation", 2.0, weatherBulletin.getPrecipitation(), DELTA);
            assertEquals("Should be equals : snow Volume", 1.0, weatherBulletin.getSnowVolume(), DELTA);
            assertNull("Should not throw error : normal bulletin", weatherBulletin.getErrorTrace());
            
	    } 
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	@Test
	public final void setWeatherInformationsNormalJSONWith1hRainAndSnow() {
	    try {
            OpenWeatherMapRequest openWeatherMapRequest = new OpenWeatherMapRequest();
            ErrorTrace errorTrace = new ErrorTrace();
            WeatherBulletin weatherBulletin= new WeatherBulletin(errorTrace);
			 //Retrieving example.json content and parse it to String
			Object content = parser.parse(new FileReader("jsonFiles/testSetWeatherInformations1h.json"));
            JSONObject jsonObject = (JSONObject) content;
			String object = jsonObject.toString();
	        Calendar cal = Calendar.getInstance();
	        String day = Integer.toString(cal.get(Calendar.YEAR)) + "-" + Integer.toString(cal.get(Calendar.MONTH)+1)
	                + "-" + Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
	        
            weatherBulletin = openWeatherMapRequest.setWeatherInformations(object);            
            assertTrue("Should be equals : city name", weatherBulletin.getCityName().equals("Talence"));
            assertTrue("Should be equals : dateFrom", weatherBulletin.getDateFrom().equals(day));
            assertTrue("Should be equals : dateTo", weatherBulletin.getDateTo().equals(day));


            assertEquals("Should be equals :min temperature", 11.0, weatherBulletin.getMinTemperature(), DELTA);			

    		assertEquals("Should be equals : max temperature", weatherBulletin.getMaxTemperature(), 13.0, DELTA);
    		assertEquals("Should be equals : wind speed", weatherBulletin.getWindSpeed(), 7.7, DELTA);
    		assertEquals("Should be equals : humidity", weatherBulletin.getHumidity(), 62.0, DELTA);
            assertEquals("Should be equals : precipitation", 2.0, weatherBulletin.getPrecipitation(), DELTA);
            assertEquals("Should be equals : snow Volume", 1.0, weatherBulletin.getSnowVolume(), DELTA);
            assertNull("Should not throw error : normal bulletin", weatherBulletin.getErrorTrace());
            
	    } 
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
/*
	@Test
	public final void testGetWeatherBulletinByInterval() {
		fail("Not yet implemented"); // TODO
	}

*/
}
