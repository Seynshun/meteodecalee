package TextGenerator;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Test;

import Exceptions.ErrorTrace;
import Parser.WeatherBulletin;

public class RunTextGeneratorTest {

	@Test
	public final void testGetGeneratorResponseWeatherBulletin() throws ParseException {
		RunTextGenerator runTextGenerator = new RunElvexV2();

        //create WeatherBulletin0 and WeatherBulletin1 with no error
        ErrorTrace errorTrace = new ErrorTrace();
        WeatherBulletin bulletin0 =  new WeatherBulletin(errorTrace);
        bulletin0.setDateFrom("2020-4-18");
        bulletin0.setDateTo("2020-4-23");
        bulletin0.setCityName("Talence");
        bulletin0.setCoordonates(44.8, -0.6);
        bulletin0.setTemperature(8.79);
        bulletin0.setMaxTemperature(10);
        bulletin0.setMinTemperature(8);
        runTextGenerator.setErrorTrace(errorTrace);
        String sentence = runTextGenerator.getGeneratorResponse(bulletin0);

        boolean isSentenceInExpectedStringList = false;
        ArrayList<String> expectedStringList = new ArrayList<String>();
        expectedStringList.add("ce Samedi 18 Avril on va crever .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir la chipolata au bain_marie .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir la raie en sauce .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir les bigorneaux qui collent aux rochers .");
		expectedStringList.add("ce Samedi 18 Avril il va faire plus chaud que sous l'aiselle d'un tondeur .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir les olives qui baignent dans l'huile .");
		expectedStringList.add("ce Samedi 18 Avril on va crever de chaud .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir les rillettes sous les bras .");
		

        isSentenceInExpectedStringList = expectedStringList.contains(sentence);

        assertEquals("Should be equals : sentence", true, isSentenceInExpectedStringList);
	}


	@Test
    public final void testGetGeneratorResponseArrayListOfWeatherBulletin() throws ParseException {
        RunTextGenerator runTextGenerator = new RunElvexV2();
        ArrayList<WeatherBulletin> bulletins= new ArrayList<WeatherBulletin>();
        ArrayList<String> sentences = new ArrayList<String>();

        //create WeatherBulletin0 and WeatherBulletin1 with no error
        ErrorTrace errorTrace = new ErrorTrace();
        WeatherBulletin bulletin0 =  new WeatherBulletin(errorTrace);
        bulletin0.setDateFrom("2020-4-18");
        bulletin0.setDateTo("2020-4-23");
        bulletin0.setCityName("Talence");
        bulletin0.setCoordonates(44.8, -0.6);
        bulletin0.setTemperature(8.79);
        bulletin0.setMaxTemperature(10);
        bulletin0.setMinTemperature(8);
        runTextGenerator.setErrorTrace(errorTrace);
        bulletins.add(bulletin0);
        bulletins.add(bulletin0);
        sentences = runTextGenerator.getGeneratorResponse(bulletins);

        boolean isSentenceInExpectedStringList = false;
        boolean isSentenceInExpectedStringList2 = false;
		ArrayList<String> expectedStringList = new ArrayList<String>();
		expectedStringList.add("ce Samedi 18 Avril on va crever .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir la chipolata au bain_marie .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir la raie en sauce .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir les bigorneaux qui collent aux rochers .");
		expectedStringList.add("ce Samedi 18 Avril il va faire plus chaud que sous l'aiselle d'un tondeur .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir les olives qui baignent dans l'huile .");
		expectedStringList.add("ce Samedi 18 Avril on va crever de chaud .");
		expectedStringList.add("ce Samedi 18 Avril on va avoir les rillettes sous les bras .");	

        isSentenceInExpectedStringList = expectedStringList.contains(sentences.get(0));
        isSentenceInExpectedStringList2 = expectedStringList.contains(sentences.get(1));

        assertEquals("Should be equals : sentences", true,isSentenceInExpectedStringList);
        assertEquals("Should be equals : sentences", true, isSentenceInExpectedStringList2);
    }

	@Test
	public final void testGetGeneratorResponseWeatherBulletinHighSnowVolume() throws ParseException {
		RunTextGenerator runTextGenerator = new RunElvexV2();

        //create WeatherBulletin0 and WeatherBulletin1 with no error
        ErrorTrace errorTrace = new ErrorTrace();
        WeatherBulletin bulletin0 =  new WeatherBulletin(errorTrace);
        bulletin0.setDateFrom("2020-4-18");
        bulletin0.setDateTo("2020-4-23");
        bulletin0.setCityName("Talence");
        bulletin0.setCoordonates(44.8, -0.6);
        bulletin0.setTemperature(8.79);
        bulletin0.setMaxTemperature(10);
        bulletin0.setMinTemperature(8);
        bulletin0.setSnowVolume(510);
        runTextGenerator.setErrorTrace(errorTrace);
        String sentence = runTextGenerator.getGeneratorResponse(bulletin0);

        boolean isSentenceInExpectedStringList = false;
        ArrayList<String> expectedStringList = new ArrayList<String>();
        expectedStringList.add("ce Samedi 18 Avril il va neiger .");
		expectedStringList.add("ce Samedi 18 Avril il allez y avoir de la neige .");
		expectedStringList.add("ce Samedi 18 Avril il vas y avoir de la neige .");
		expectedStringList.add("ce Samedi 18 Avril il allons y avoir de la neige .");
		expectedStringList.add("ce Samedi 18 Avril il va y avoir de la neige .");
		expectedStringList.add("ce Samedi 18 Avril il vont y avoir de la neige .");
		expectedStringList.add("ce Samedi 18 Avril il vais y avoir de la neige .");
		expectedStringList.add("ce Samedi 18 Avril il va y avoir de la neige .");
        isSentenceInExpectedStringList = expectedStringList.contains(sentence);

        assertEquals("Should be equals : sentence", true, isSentenceInExpectedStringList);
	}
	
	@Test
	public final void testGetGeneratorResponseWeatherBulletinHighPrecipitation() throws ParseException {
		RunTextGenerator runTextGenerator = new RunElvexV2();

        //create WeatherBulletin0 and WeatherBulletin1 with no error
        ErrorTrace errorTrace = new ErrorTrace();
        WeatherBulletin bulletin0 =  new WeatherBulletin(errorTrace);
        bulletin0.setDateFrom("2020-4-18");
        bulletin0.setDateTo("2020-4-23");
        bulletin0.setCityName("Talence");
        bulletin0.setCoordonates(44.8, -0.6);
        bulletin0.setTemperature(8.79);
        bulletin0.setMaxTemperature(10);
        bulletin0.setMinTemperature(8);
        bulletin0.setPrecipitation(9);
        runTextGenerator.setErrorTrace(errorTrace);
        String sentence = runTextGenerator.getGeneratorResponse(bulletin0);

        boolean isSentenceInExpectedStringList = false;
        ArrayList<String> expectedStringList = new ArrayList<String>();
		try {
			Scanner scanner = new Scanner(new File("jsonFiles/ElvexSentences_HighPrecipitation.txt"));
			while (scanner.hasNextLine()) {
				expectedStringList.add(scanner.nextLine().replaceAll("\\s+",""));
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        for (String u : expectedStringList) {
        	if(u.equals(sentence.replaceAll("\\s+",""))) {
        		isSentenceInExpectedStringList = true;
        	}
        }
        assertEquals("Should be equals : sentence", true, isSentenceInExpectedStringList);
	}
	
	@Test
	public final void testGetGeneratorResponseWeatherBulletinHighTemperature() throws ParseException {
		RunTextGenerator runTextGenerator = new RunElvexV2();

        //create WeatherBulletin0 and WeatherBulletin1 with no error
        ErrorTrace errorTrace = new ErrorTrace();
        WeatherBulletin bulletin0 =  new WeatherBulletin(errorTrace);
        bulletin0.setDateFrom("2020-4-18");
        bulletin0.setDateTo("2020-4-23");
        bulletin0.setCityName("Talence");
        bulletin0.setCoordonates(44.8, -0.6);
        bulletin0.setTemperature(25);
        bulletin0.setMaxTemperature(26);
        bulletin0.setMinTemperature(24);
        runTextGenerator.setErrorTrace(errorTrace);
        String sentence = runTextGenerator.getGeneratorResponse(bulletin0);
        boolean isSentenceInExpectedStringList = false;
        ArrayList<String> expectedStringList = new ArrayList<String>();
		try {
			Scanner scanner = new Scanner(new File("jsonFiles/ElvexSentences_HighTemperature.txt"));
			while (scanner.hasNextLine()) {
				expectedStringList.add(scanner.nextLine().replaceAll("\\s+",""));
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
        for (String u : expectedStringList) {
			String tmp =u.replaceAll("\\s+","");
        	if(tmp.equals(sentence.replaceAll("\\s+",""))) {
        		isSentenceInExpectedStringList = true;
        	}
        }
		
        assertEquals("Should be equals : sentence", true, isSentenceInExpectedStringList);
	}
	
	@Test
	public final void testGetGeneratorResponseWeatherBulletinLowTemperature() throws ParseException {
		RunTextGenerator runTextGenerator = new RunElvexV2();

        //create WeatherBulletin0 and WeatherBulletin1 with no error
        ErrorTrace errorTrace = new ErrorTrace();
        WeatherBulletin bulletin0 =  new WeatherBulletin(errorTrace);
        bulletin0.setDateFrom("2020-4-18");
        bulletin0.setDateTo("2020-4-23");
        bulletin0.setCityName("Talence");
        bulletin0.setCoordonates(44.8, -0.6);
        bulletin0.setTemperature(-15);
        bulletin0.setMaxTemperature(-10);
        bulletin0.setMinTemperature(-20);
        runTextGenerator.setErrorTrace(errorTrace);
        String sentence = runTextGenerator.getGeneratorResponse(bulletin0);
        boolean isSentenceInExpectedStringList = false;
        ArrayList<String> expectedStringList = new ArrayList<String>();
		try {
			Scanner scanner = new Scanner(new File("jsonFiles/ElvexSentences_LowTemperature.txt"));
			while (scanner.hasNextLine()) {
				expectedStringList.add(scanner.nextLine().replaceAll("\\s+",""));
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
        for (String u : expectedStringList) {
			String tmp =u.replaceAll("\\s+","");
        	if(tmp.equals(sentence.replaceAll("\\s+",""))) {
        		isSentenceInExpectedStringList = true;
        	}
        }
		
        assertEquals("Should be equals : sentence", true, isSentenceInExpectedStringList);
	}
	
	@Test
	public final void testSetErrorTrace() {
        RunTextGenerator runTextGenerator = new RunElvexV2();
        ErrorTrace errorTrace = new ErrorTrace();
        errorTrace.setError("Erreur");
        runTextGenerator.setErrorTrace(errorTrace);
        assertEquals("Should be equals", "Erreur", runTextGenerator.getErrorTrace().getError());
	}

	@Test
	public final void testErrorMessage() {
        RunTextGenerator runTextGenerator = new RunElvexV2();
        runTextGenerator.setErrorTrace(new ErrorTrace());
        runTextGenerator.errorMessage("Erreur2");
        assertEquals("Should be equals", "Erreur2", runTextGenerator.getErrorTrace().getError());
	}

	@Test
	public final void testWarningMessage() {
        RunTextGenerator runTextGenerator = new RunElvexV2();
        runTextGenerator.setErrorTrace(new ErrorTrace());
        runTextGenerator.warningMessage("Warning");
        assertEquals("Should be equals", "Warning", runTextGenerator.getErrorTrace().getWarnings().get(0));
	}

	@Test
	public final void testClone() {
        RunTextGenerator runTextGenerator = new RunElvexV2();
        Object clone = runTextGenerator.clone();
        ((RunTextGenerator) clone).setErrorTrace(new ErrorTrace());
        ((RunTextGenerator) clone).warningMessage("Warning");
        assertEquals("Should be equals", "Warning", ((RunTextGenerator) clone).getErrorTrace().getWarnings().get(0));
	}
}
