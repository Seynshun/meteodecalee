package activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;


import java.io.IOException;
import java.util.StringTokenizer;

import androidx.appcompat.app.AppCompatActivity;

import model.R;

/**
 * The ChangeCityActivity corresponds to the city selection page and the processes that derive from it.
 */
public class ChangeCityActivity extends AppCompatActivity {
    private static ChangeCityActivity instance;
    ListView mListVille;
    ArrayList<String> numberList = new ArrayList<>();
    Button search;
    EditText research;

    public static ChangeCityActivity mainInstance(){
        return instance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_city_screen);
        instance = this;
        mListVille = findViewById(R.id.liste_ville);
        search = findViewById(R.id.button_recherche);
        research = findViewById(R.id.recherche);
        
        setTheme(R.style.ChangeCityTheme);
        numberList = MainActivity.mainInstance().getFranceCities();
        Collections.sort(numberList);
        final ArrayAdapter<String> item = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, numberList);
        mListVille.setAdapter(item);

        // when the user clicks on a city
        mListVille.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (view != null){ //hide the soft keyboard
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(),0);
                }
                setContentView(R.layout.splash_screen);
                String nameAndZip = (String) adapterView.getItemAtPosition(position);
                StringTokenizer token = new StringTokenizer(nameAndZip, ":");
                String name = token.nextToken();
                name = name.trim();
                String zip = token.nextToken();
                zip = zip.trim();
                String coordinates = findCoordinates(name, zip);
                StringTokenizer tokenCoordinate = new StringTokenizer(coordinates, "/");
                String latitude = tokenCoordinate.nextToken();
                String longitude = tokenCoordinate.nextToken();

                Intent intent = new Intent(ChangeCityActivity.this, MainActivity.class);
                intent.putExtra("gps",false);
                intent.putExtra("longitude",Double.valueOf(longitude));
                intent.putExtra("latitude",Double.valueOf(latitude));
                startActivityForResult(intent,1);
                overridePendingTransition(0,0);
            }
        });


        // when the user clicks on the research button
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String villeRecherche = research.getText().toString();
                ArrayList<String> newList = researchByName(villeRecherche);
                Collections.sort(newList);
                mListVille = findViewById(R.id.liste_ville);
                ArrayAdapter<String> item = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1, newList);
                mListVille.setAdapter(item);
            }
        });

    }

    /**
     * Search in the json the city typed by the user in the search bar
     * @param cityName the city typed by the user in the search bar
     * @return A table with the cities corresponding with the user's search
     */
    public ArrayList<String> researchByName(String cityName){
        ArrayList<String> newCity = new ArrayList<>();

        if(cityName.isEmpty()){
            newCity = numberList;
        }
        else{
            String json;
            try{
                InputStream is = getAssets().open("cities.json");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                json = new String(buffer, "UTF-8");
                JSONArray jsonArray = new JSONArray(json);
                for(int i = 0 ; i<jsonArray.length() ; i++){
                    JSONObject obj = jsonArray.getJSONObject(i);
                    if(obj.getString("name").equals(cityName) || obj.getString("slug").equals(cityName) || obj.getString("name").startsWith(cityName) || obj.getString("slug").startsWith(cityName) || obj.getString("zip_code").equals(cityName) || obj.getString("zip_code").startsWith(cityName)){
                        newCity.add(obj.getString("name") + " : " + obj.getString("zip_code"));
                    }
                }
            }catch(IOException | JSONException e){
                e.printStackTrace();
            }

            if(newCity.size() == 0){
                newCity.add("Ville inconnue");
            }
        }

        return newCity;
    }


    /**
     * Finds the GPS coordinates of a city and a given zip code
     * @param cityName city whose coordinates we want
     * @param zip zip code whose coordinates we want
     * @return GPS coordinates in the format "latitude/longitude"
     */
    public String findCoordinates(String cityName, String zip){

        String coordinates = "";
        String json;

        try{
            InputStream is = getAssets().open("cities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
            JSONArray jsonArray = new JSONArray(json);
            for(int i = 0 ; i<jsonArray.length() ; i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                if(obj.getString("name").equals(cityName) && obj.getString("zip_code").equals(zip)){
                    coordinates = obj.getString("gps_lat") + "/" + obj.getString("gps_lng");
                }
            }
        }catch(IOException | JSONException e){
            e.printStackTrace();
        }

        return coordinates;
    }

}
