package activity;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

/**
 * The ServerDown class allows you to create a dialog box indicating that the server is not responding with a request to close the application.
 */
public class DialogAlert extends AppCompatActivity {

    Context ctx;

    public DialogAlert(Context ctx) {
        this.ctx = ctx;
    }

    /**
     * Displays a dialog box indicating that the server is unavailable
     */
    public void serverDownAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setTitle("Erreur");
        alert.setMessage("Le serveur ne répond pas. Veuillez réssayer plus tard.").setCancelable(false).setPositiveButton("Fermer l'application", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    /**
     * Displays a custom dialog box forcing the user to close the application
     * @param title the title of the dialog box
     * @param message the message of the dialog box
     */
    public void errorDialog(String title, String message) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setTitle(title);
        alert.setMessage(message).setCancelable(false).setPositiveButton("Fermer l'application", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    /**
     * Displays a custom dialog box for warnings message
     * @param title the title of the dialog box
     * @param message the message of the dialog box
     */
    public void warningDialog(String title, String message) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setTitle(title);
        alert.setMessage(message).setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    /**
     * Displays a dialog box indicating that the application is using vulgar language and asking the user to accept or exit.
     * @param preferences saves the user's choice not to display the message if he has already accepted once
     */
    public void vulgarityDialog(final SharedPreferences preferences) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setTitle("Avertissement");
        alert.setMessage("L'application \"Météo Décalée\" peut parfois utiliser un langage vulgaire.").setCancelable(false).setNegativeButton("Continuer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("vulgarity", true);
                editor.commit();

            }
        }).setPositiveButton("Fermer l'application", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        AlertDialog dialog = alert.create();
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP ;
        dialog.show();
    }
}