package activity;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import model.R;
import weather.WeatherBulletin;
import weather.WeatherJsonParser;
import weather.WeatherParser;
import network.LocationBroadcastReceiver;
import network.LocationService;

import static java.util.Calendar.*;

public class MainActivity extends AppCompatActivity {

    private static MainActivity instance;
    private static double defaultLongitude = 2.3488;
    private static double defaultLatitude = 48.8534;

    LocationBroadcastReceiver receiver = new LocationBroadcastReceiver();
    SharedPreferences myPrefs;
    TextView sentence, temperature, city;
    TableLayout tablePrediction;
    TableRow tableDay2, tableDay3, tableDay4, tableDay5, tableDay6, trHourDay2, trTempDay2, trHourDay3, trTempDay3, trHourDay4, trTempDay4, trHourDay5, trTempDay5, trHourDay6, trTempDay6;
    TextView day2, day3, day4, day5, day6, tempMinMaxDay2, tempMinMaxDay3, tempMinMaxDay4, tempMinMaxDay5, tempMinMaxDay6, textHourDay2, textTempDay2, textHourDay3, textTempDay3, textHourDay4, textTempDay4, textHourDay5, textTempDay5, textHourDay6, textTempDay6;
    ArrayList<String> franceCities = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.SplashTheme);
        setContentView(R.layout.splash_screen);
        myPrefs = getSharedPreferences("prefID", Context.MODE_PRIVATE);
        instance = this;

        Intent intent = getIntent();
        boolean gps = intent.getBooleanExtra("gps", true);

        // if gps is enabled
        if (gps == true) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    //request location permission
                    startService();
                }
            } else {
                //start the location service
                startService();
            }
        // if gps is disabled
        } else {
            try {
                receiver.startConnection(intent.getDoubleExtra("longitude", defaultLongitude), intent.getDoubleExtra("latitude", defaultLatitude));
            } catch (ExecutionException | InterruptedException | ParseException | JSONException  e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Get the json sent by the server and manage what to display or not to the user
     */
    public void responseHandler() throws JSONException, ParseException {
        String response = receiver.getResponse();
        DialogAlert alert = new DialogAlert(this);
        myPrefs = getSharedPreferences("prefID", Context.MODE_PRIVATE);
        boolean vulgarityAccepted = myPrefs.getBoolean("vulgarity", false);

        if (vulgarityAccepted == false) {
            alert.vulgarityDialog(myPrefs);
        }
        if (response == "failed") { // if the connection has failed, this is indicated to the user
            final Context ctx = this;
            DialogAlert dialog = new DialogAlert(ctx);
            dialog.serverDownAlert();

        } else { // if the connection is successful, we update the weather data.

            WeatherParser js = new WeatherJsonParser(response); // parse the weather data and store them in a WeatherBulletin
            WeatherBulletin bulletin = ((WeatherJsonParser) js).getBulletin(); //get the weather data parsed


            if (areErrors(bulletin) == false) {
                areWarnings(bulletin);
                setScene();
                updateTextView(bulletin);
                franceCities = setFranceCitiesData();
            }

        }
    }

    /**
     * Exit the loading screen to display the application's weather page
     */
    private void setScene() {
        setContentView(R.layout.activity_main);
        sentence = findViewById(R.id.msgElvex);
        temperature = findViewById(R.id.temperature);
        city = findViewById(R.id.city);
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openChangeCityScreen();
            }
        });

        tablePrediction = findViewById(R.id.tablePrediction);

        /**
         * Make the link with the layouts of the activity_main.xml file for every day
         */
        day2 = findViewById(R.id.day2);
        tableDay2 = findViewById(R.id.tableDay2);
        tempMinMaxDay2 = findViewById(R.id.tempMinMaxDay2);
        trHourDay2 = findViewById(R.id.trHourDay2);
        trTempDay2 = findViewById(R.id.trTempDay2);

        day3 = findViewById(R.id.day3);
        tableDay3 = findViewById(R.id.tableDay3);
        tempMinMaxDay3 = findViewById(R.id.tempMinMaxDay3);
        trHourDay3 = findViewById(R.id.trHourDay3);
        trTempDay3 = findViewById(R.id.trTempDay3);

        day4 = findViewById(R.id.day4);
        tableDay4 = findViewById(R.id.tableDay4);
        tempMinMaxDay4 = findViewById(R.id.tempMinMaxDay4);
        trHourDay4 = findViewById(R.id.trHourDay4);
        trTempDay4 = findViewById(R.id.trTempDay4);

        day5 = findViewById(R.id.day5);
        tableDay5 = findViewById(R.id.tableDay5);
        tempMinMaxDay5 = findViewById(R.id.tempMinMaxDay5);
        trHourDay5 = findViewById(R.id.trHourDay5);
        trTempDay5 = findViewById(R.id.trTempDay5);

        day6 = findViewById(R.id.day6);
        tableDay6 = findViewById(R.id.tableDay6);
        tempMinMaxDay6 = findViewById(R.id.tempMinMaxDay6);
        trHourDay6 = findViewById(R.id.trHourDay6);
        trTempDay6 = findViewById(R.id.trTempDay6);
    }

    /**
     * Updates the weather data displayed on the screen from the json file issued by the server
     */
    public void updateTextView(WeatherBulletin bulletin) {


        sentence.setText(bulletin.get_sentence());
        String temp = Double.toString(bulletin.get_temp());
        String[] tempSplit = temp.split("\\.");
        final String degree = "°C";
        String tempFinal = tempSplit[0].concat(degree);
        temperature.setText(tempFinal);
        String cityName = bulletin.get_cityName();
        city.setText(cityName);

        TextView tableHour, tableTemp;
        TableRow trHour, trTemp;

        //Create the tablerows
        trHour = new TableRow(this);
        trTemp = new TableRow(this);

        Resources res = this.getResources();

        /**
         * Collect and display the temperatures every 3 hours in a table
         */
        for (Map.Entry<String, Double> tablePrediction : bulletin.get_first24hour().entrySet()) {
            String key = tablePrediction.getKey();

            // Here create the TextView dynamically
            tableHour = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1);
            tableHour.setGravity(Gravity.CENTER);
            tableHour.setText(key + ":00");
            paramsExample.setMargins(5, 20, 5, 20);
            tableHour.setPadding(5, 20, 5, 20);
            tableHour.setLayoutParams(paramsExample);
            trHour.addView(tableHour);
        }

        trHour.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trHour.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trHour.setBackground(res.getDrawable(R.drawable.cell_shape));
        tablePrediction.addView(trHour);

        for (Map.Entry<String, Double> tablePrediction : bulletin.get_first24hour().entrySet()) {
            long value = Math.round(tablePrediction.getValue());

            // Here create the TextView dynamically
            tableTemp = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1);
            tableTemp.setGravity(Gravity.CENTER);
            tableTemp.setText(value + degree);
            paramsExample.setMargins(10, 50, 10, 50);
            tableTemp.setPadding(10, 20, 10, 20);
            tableTemp.setLayoutParams(paramsExample);
            trTemp.addView(tableTemp);
        }
        trTemp.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trTemp.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trTemp.setBackground(res.getDrawable(R.drawable.cell_shape));
        tablePrediction.addView(trTemp);

        /**
         * Collect minimum and maximum temperatures in two ArrayList
         */

        ArrayList<String> tableDay = new ArrayList<>();
        ArrayList<Long> tableTempMinByDay = new ArrayList<>();
        ArrayList<Long> tableTempMaxByDay = new ArrayList<>();

        for (Map.Entry<String, Double> tempMinByDay : bulletin.get_temperatureMinByDay().entrySet()) {
            Long tempMin = Math.round(tempMinByDay.getValue());
            tableDay.add(tempMinByDay.getKey());
            tableTempMinByDay.add(tempMin);
        }

        for (Map.Entry<String, Double> tempMaxByDay : bulletin.get_temperatureMaxByDay().entrySet()) {
            Long tempMax = Math.round(tempMaxByDay.getValue());
            tableTempMaxByDay.add(tempMax);
        }

        Calendar c;
        Date date;
        SimpleDateFormat dateFormat;
        String strDate;
        c = getInstance();
        dateFormat = new SimpleDateFormat("dd MMMM", Locale.FRANCE);

        /**
         * Collect and put in five different tables all the temperatures for each day and hide them until the user clicks on a day
         */
        c.add(DATE, 1);
        date = c.getTime();
        strDate = dateFormat.format(date);
        day2.setText(tableDay.get(1)+" "+strDate);
        tempMinMaxDay2.setText("Min "+tableTempMinByDay.get(1)+degree+"/Max "+tableTempMaxByDay.get(1)+degree);
        for (Map.Entry<String, Double> tableDay2 : bulletin.get_day2().entrySet()) {
            String hourKey = tableDay2.getKey();

            // Here create the TextView dynamically
            textHourDay2 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textHourDay2.setGravity(Gravity.CENTER);
            textHourDay2.setText(hourKey + ":00");
            paramsExample.setMargins(10, 50, 10, 50);
            textHourDay2.setLayoutParams(paramsExample);
            trHourDay2.addView(textHourDay2);
        }
        trHourDay2.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trHourDay2.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trHourDay2.setBackground(res.getDrawable(R.drawable.cell_shape));
        trHourDay2.setVisibility(View.GONE);

        for (Map.Entry<String, Double> tableDay2 : bulletin.get_day2().entrySet()) {
            long value = Math.round(tableDay2.getValue());

            // Here create the TextView dynamically
            textTempDay2 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textTempDay2.setGravity(Gravity.CENTER);
            textTempDay2.setText(value+degree);
            paramsExample.setMargins(10, 50, 10, 50);
            textTempDay2.setLayoutParams(paramsExample);
            trTempDay2.addView(textTempDay2);
        }
        trTempDay2.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trTempDay2.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trTempDay2.setBackground(res.getDrawable(R.drawable.cell_shape));
        trTempDay2.setVisibility(View.GONE);

        tableDay2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                trHourDay2.setVisibility(trHourDay2.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                trTempDay2.setVisibility(trTempDay2.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });

        c.add(DATE, 1);
        date = c.getTime();
        strDate = dateFormat.format(date);
        day3.setText(tableDay.get(2)+" "+strDate);
        tempMinMaxDay3.setText("Min "+tableTempMinByDay.get(2)+degree+"/Max "+tableTempMaxByDay.get(2)+degree);
        for (Map.Entry<String, Double> tableDay3 : bulletin.get_day3().entrySet()) {
            String hourKey = tableDay3.getKey();

            // Here create the TextView dynamically
            textHourDay3 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textHourDay3.setGravity(Gravity.CENTER);
            textHourDay3.setText(hourKey + ":00");
            paramsExample.setMargins(10, 50, 10, 50);
            textHourDay3.setLayoutParams(paramsExample);
            trHourDay3.addView(textHourDay3);
        }
        trHourDay3.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trHourDay3.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trHourDay3.setBackground(res.getDrawable(R.drawable.cell_shape));
        trHourDay3.setVisibility(View.GONE);

        for (Map.Entry<String, Double> tableDay3 : bulletin.get_day3().entrySet()) {
            long value = Math.round(tableDay3.getValue());

            // Here create the TextView dynamically
            textTempDay3 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textTempDay3.setGravity(Gravity.CENTER);
            textTempDay3.setText(value+degree);
            paramsExample.setMargins(10, 50, 10, 50);
            textTempDay3.setLayoutParams(paramsExample);
            trTempDay3.addView(textTempDay3);
        }
        trTempDay3.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trTempDay3.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trTempDay3.setBackground(res.getDrawable(R.drawable.cell_shape));
        trTempDay3.setVisibility(View.GONE);

        tableDay3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                trHourDay3.setVisibility(trHourDay3.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                trTempDay3.setVisibility(trTempDay3.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });

        c.add(DATE, 1);
        date = c.getTime();
        strDate = dateFormat.format(date);
        day4.setText(tableDay.get(3)+" "+strDate);
        tempMinMaxDay4.setText("Min "+tableTempMinByDay.get(3)+degree+"/Max "+tableTempMaxByDay.get(3)+degree);
        for (Map.Entry<String, Double> tableDay4 : bulletin.get_day4().entrySet()) {
            String hourKey = tableDay4.getKey();

            // Here create the TextView dynamically
            textHourDay4 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textHourDay4.setGravity(Gravity.CENTER);
            textHourDay4.setText(hourKey + ":00");
            paramsExample.setMargins(10, 50, 10, 50);
            textHourDay4.setLayoutParams(paramsExample);
            trHourDay4.addView(textHourDay4);
        }
        trHourDay4.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trHourDay4.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trHourDay4.setBackground(res.getDrawable(R.drawable.cell_shape));
        trHourDay4.setVisibility(View.GONE);

        for (Map.Entry<String, Double> tableDay4 : bulletin.get_day4().entrySet()) {
            long value = Math.round(tableDay4.getValue());

            // Here create the TextView dynamically
            textTempDay4 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textTempDay4.setGravity(Gravity.CENTER);
            textTempDay4.setText(value+degree);
            paramsExample.setMargins(10, 50, 10, 50);
            textTempDay4.setLayoutParams(paramsExample);
            trTempDay4.addView(textTempDay4);
        }
        trTempDay4.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trTempDay4.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trTempDay4.setBackground(res.getDrawable(R.drawable.cell_shape));
        trTempDay4.setVisibility(View.GONE);

        tableDay4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                trHourDay4.setVisibility(trHourDay4.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                trTempDay4.setVisibility(trTempDay4.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });


        c.add(DATE, 1);
        date = c.getTime();
        strDate = dateFormat.format(date);
        day5.setText(tableDay.get(4)+" "+strDate);
        tempMinMaxDay5.setText("Min "+tableTempMinByDay.get(4)+degree+"/Max "+tableTempMaxByDay.get(4)+degree);
        for (Map.Entry<String, Double> tableDay5 : bulletin.get_day5().entrySet()) {
            String hourKey = tableDay5.getKey();

            // Here create the TextView dynamically
            textHourDay5 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textHourDay5.setGravity(Gravity.CENTER);
            textHourDay5.setText(hourKey + ":00");
            paramsExample.setMargins(10, 50, 10, 50);
            textHourDay5.setLayoutParams(paramsExample);
            trHourDay5.addView(textHourDay5);
        }
        trHourDay5.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trHourDay5.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trHourDay5.setBackground(res.getDrawable(R.drawable.cell_shape));
        trHourDay5.setVisibility(View.GONE);

        for (Map.Entry<String, Double> tableDay5 : bulletin.get_day5().entrySet()) {
            long value = Math.round(tableDay5.getValue());

            // Here create the TextView dynamically
            textTempDay5 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textTempDay5.setGravity(Gravity.CENTER);
            textTempDay5.setText(value+degree);
            paramsExample.setMargins(10, 50, 10, 50);
            textTempDay5.setLayoutParams(paramsExample);
            trTempDay5.addView(textTempDay5);
        }
        trTempDay5.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trTempDay5.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trTempDay5.setBackground(res.getDrawable(R.drawable.cell_shape));
        trTempDay5.setVisibility(View.GONE);

        tableDay5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                trHourDay5.setVisibility(trHourDay5.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                trTempDay5.setVisibility(trTempDay5.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });

        c.add(DATE, 1);
        date = c.getTime();
        strDate = dateFormat.format(date);
        day6.setText(tableDay.get(5)+" "+strDate);
        tempMinMaxDay6.setText("Min "+tableTempMinByDay.get(5)+degree+"/Max "+tableTempMaxByDay.get(5)+degree);
        for (Map.Entry<String, Double> tableDay6 : bulletin.get_day6().entrySet()) {
            String hourKey = tableDay6.getKey();

            // Here create the TextView dynamically
            textHourDay6 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textHourDay6.setGravity(Gravity.CENTER);
            textHourDay6.setText(hourKey + ":00");
            paramsExample.setMargins(10, 50, 10, 50);
            textHourDay6.setLayoutParams(paramsExample);
            trHourDay6.addView(textHourDay6);
        }
        trHourDay6.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trHourDay6.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trHourDay6.setBackground(res.getDrawable(R.drawable.cell_shape));
        trHourDay6.setVisibility(View.GONE);

        for (Map.Entry<String, Double> tableDay6 : bulletin.get_day6().entrySet()) {
            long value = Math.round(tableDay6.getValue());

            // Here create the TextView dynamically
            textTempDay6 = new TextView(this);
            TableRow.LayoutParams paramsExample = new TableRow.LayoutParams(800, TableRow.LayoutParams.MATCH_PARENT, 1);
            textTempDay6.setGravity(Gravity.CENTER);
            textTempDay6.setText(value+degree);
            paramsExample.setMargins(10, 50, 10, 50);
            textTempDay6.setLayoutParams(paramsExample);
            trTempDay6.addView(textTempDay6);
        }
        trTempDay6.setShowDividers(TableRow.SHOW_DIVIDER_MIDDLE);
        trTempDay6.setDividerDrawable(res.getDrawable(R.color.colorPrimaryDark));
        trTempDay6.setBackground(res.getDrawable(R.drawable.cell_shape));
        trTempDay6.setVisibility(View.GONE);

        tableDay6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                trHourDay6.setVisibility(trHourDay6.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                trTempDay6.setVisibility(trTempDay6.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });
    }

    /**
     * Checks if there is an error in the WeatherBulletin, this error is present in the json emitted by the server if it encountered one during the exchange with the API or the Elvex process.
     * If there is an error, the user is informed.
     *
     * @param bulletin
     */
    public boolean areErrors(WeatherBulletin bulletin) {
        if (bulletin.get_error() != null) {
            DialogAlert alert = new DialogAlert(this);
            String phrase = bulletin.get_error() + "\n";
            if (bulletin.get_warnings() != null) {
                ArrayList<String> warnings = new ArrayList<>(bulletin.get_warnings());
                for (String w : warnings) {
                    phrase += w + "\n";
                }

            }
            alert.errorDialog("Erreur", phrase);
            return true;
        }
        ;
        return false;
    }


    /**
     * Checks if there are warnings in the WeatherBulletin.
     * A warning is an error that does not prevent the application from working but can lead to false information.
     * If there is a warning, the user is informed.
     *
     * @param bulletin
     */
    public boolean areWarnings(WeatherBulletin bulletin) {
        if (bulletin.get_warnings() != null) {
            ArrayList<String> warnings = new ArrayList<>(bulletin.get_warnings());
            String phrase = "";
            for (String w : warnings) {
                phrase += w + "\n";
            }
            DialogAlert alert = new DialogAlert(this);
            alert.warningDialog("Avertissement", phrase);
            return true;
        }
        return false;
    }

    public void startService() {
        IntentFilter filter = new IntentFilter("ACT_LOC");
        registerReceiver(receiver, filter);
        Intent intent = new Intent(MainActivity.this, LocationService.class);
        startService(intent);
    }

    /**
     * Request to activate GPS localization, if refused we load Paris weather by default
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startService();
            } else {
                try {
                    receiver.startConnection(defaultLongitude, defaultLatitude);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Displays the city selection page by starting the corresponding activity (ChangeCityActivity)
     */
    public void openChangeCityScreen() {
        Intent intent = new Intent(this, ChangeCityActivity.class);
        startActivity(intent);
    }

    /**
     * Retrieves all the french cities of json.
     * As the search takes some time, it is performed when opening the application during the loading screen rather than when opening the city selection page.
     * @return the table containing the cities
     */
    public ArrayList<String> setFranceCitiesData() {
        ArrayList<String> numberList = new ArrayList<>();
        String json;
        try {
            InputStream is = getAssets().open("cities.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                numberList.add(obj.getString("name") + " : " + obj.getString("zip_code"));
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return numberList;
    }

    public static MainActivity mainInstance() {
        return instance;
    }

    public ArrayList<String> getFranceCities() {
        return franceCities;
    }

}