package network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.json.JSONException;

import java.text.ParseException;
import java.util.concurrent.ExecutionException;

import activity.MainActivity;

/**
 * The LocationBroadcastReceiver class manages the reception of GPS data and the launching of the network connection with the server
 */
public class LocationBroadcastReceiver extends BroadcastReceiver {

    private String response;

    /**
     * Retrieves GPS coordinates and initiates a connection with the server
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("ACT_LOC")) {
            double latitude = intent.getDoubleExtra("lat", 0);
            double longitude = intent.getDoubleExtra("lon", 0);

            try {
                startConnection(longitude, latitude);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException | JSONException | ParseException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Launches the network connection with the server and retrieves the json emitted by the server.
     * @param longitude GPS coordinate to be sent to the server
     * @param latitude GPS coordinate to be sent to the server
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public void startConnection(double longitude, double latitude) throws ExecutionException, InterruptedException, JSONException, ParseException {
        Network nt = new Network();
        nt.setCoordinate(longitude, latitude);
        response = nt.execute().get();
        setResponse(response);
        MainActivity.mainInstance().responseHandler();
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}