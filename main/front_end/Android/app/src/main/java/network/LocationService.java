package network;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import androidx.annotation.Nullable;

/**
 * The LocationService allows to launch queries to obtain GPS coordinates of the smartphone
 */
public class LocationService extends Service{
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationCallback locationCallback;
    private double latitude;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        //initialization of the location callback
        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult){
                super.onLocationResult(locationResult);
                //Log.d("myLog", "Lat is: " + locationResult.getLastLocation().getLatitude() + " , " + "Long:" + locationResult.getLastLocation().getLongitude());
                Intent intent = new Intent("ACT_LOC");
                intent.putExtra("lat", locationResult.getLastLocation().getLatitude());
                intent.putExtra("lon", locationResult.getLastLocation().getLongitude());
                latitude = locationResult.getLastLocation().getLatitude();
                sendBroadcast(intent);
            }
        };
    }

    /**
     * This function permit to know if a service is started
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        requestLocation();
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * Initialize a requestLocation
     * Permit to send a request location to the smartphone and get back the gps location
     * In the function setPriority() we used PRIORITY_BALANCED_POWER_ACCURACY because
     * it uses GPS only when accessing location updates from any other provider is not possible
     * The function requestLocationUpdates() permit to update the latitude and longitude in the function
     * onCreate()
     */
    private void requestLocation(){
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }
}
