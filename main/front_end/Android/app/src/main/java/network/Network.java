package network;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import java.io.ObjectOutputStream;
import java.io.OutputStream;


import java.net.InetSocketAddress;
import java.net.Socket;

import weather.WeatherJsonParser;
import weather.WeatherParser;

/**
 * The Network class establishes a connection to a server and manages the sending and receiving of network messages.
 */

public class Network extends AsyncTask<Void, Void, String> {
    /**
     * server IP address
     */
    private static final String IP = "104.40.195.32";
    /**
     * server port
     */
    private static final int PORT = 4400;

    Socket s;

    /**
     * coordinates to be sent to the server
     */
    private double latitude;
    private double longitude;

    public void setCoordinate(double longitude, double latitude){
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    protected String doInBackground(Void... voids) {
        String response = null;
        try {
                s = new Socket();
                s.connect(new InetSocketAddress(IP, PORT), 2000);

        } catch (IOException e) {
            e.printStackTrace();
           return "failed";
        }
        sendMsg(s);
        response = listenMsg(s);
        return response;
    }

    /**
     * Send a Json file to the server
     * @param s
     */
    public void sendMsg(Socket s) {
        try {
            WeatherParser json = new WeatherJsonParser();
            JSONObject object = ((WeatherJsonParser) json).toJson(longitude, latitude);
            OutputStream out = s.getOutputStream();
            ObjectOutputStream obj = new ObjectOutputStream(out);
            obj.writeObject(object.toString());
            out.flush();

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the json file sent by the server
     * @param s
     * @return the json in string format
     */
    public String listenMsg(Socket s) {
        String response = null;
        try {
            InputStream in = s.getInputStream();
            ObjectInputStream obj = new ObjectInputStream(in);
            response = (String) obj.readObject();
            obj.close();

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}