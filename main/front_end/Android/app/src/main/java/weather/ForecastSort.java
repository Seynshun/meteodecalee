package weather;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import weather.WeatherBulletin;

/**
 * The ForecastSort class sorts the weather forecast day by day
 */
public class ForecastSort {

    private final int nbTemperatureByDay = 8;

    /**
     * takes a hashMap containing hourly weather forecasts for 5 days to separate them day by day and fill in the various WeatherBulletin hashMaps. One hashMap per day
     * @param _hourlyWeather The initial hashMap containing the weather forecast every 3 hours
     * @param bulletin The WeatherBulletin bulletin to be modified by adding the forecasts
     * @throws ParseException
     */
    public void daySort( LinkedHashMap<String, Double> _hourlyWeather, WeatherBulletin bulletin ) throws ParseException {

        int i = 0, j = 0, k= 0, l = 0, m = 0, n = 0;

        Date today = new Date();
        String modifiedToday = new SimpleDateFormat("yyyy-MM-dd").format(today); // today date in yyyy-MM-dd format
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(modifiedToday));
        c.add(Calendar.DATE, 1);
        String day2 = sdf.format(c.getTime()); // date of tomorrow day (day 2)
        c.add(Calendar.DATE, 1);
        String day3 = sdf.format(c.getTime()); // date of after tomorrow (day 3)
        c.add(Calendar.DATE, 1);
        String day4 = sdf.format(c.getTime()); // date of day 4)
        c.add(Calendar.DATE, 1);
        String day5 = sdf.format(c.getTime()); // date of day 5

        for (Map.Entry<String,Double> entry : _hourlyWeather.entrySet()) {

            String key = entry.getKey();
            String[] hourDaySeparator = key.split(" ");
            double value = entry.getValue();

            if (i < nbTemperatureByDay){
                bulletin.set_first24hour(reformatHour(hourDaySeparator[1]), value);
                i++;
            }
            if (!hourDaySeparator[0].equals(modifiedToday) && j < nbTemperatureByDay){ // if that's the next day, we fill his hashMap for nbTemperatureByDay next hours
                bulletin.set_day2(reformatHour(hourDaySeparator[1]), value);
                j++;
            }
            if(!hourDaySeparator[0].equals(day2) && k < nbTemperatureByDay && j == nbTemperatureByDay){ // likewise, but checking to see if we've already spent the day before
                bulletin.set_day3(reformatHour(hourDaySeparator[1]), value);
                k++;
            }
            if(!hourDaySeparator[0].equals(day3) && l < nbTemperatureByDay && k == nbTemperatureByDay){
                bulletin.set_day4(reformatHour(hourDaySeparator[1]), value);
                l++;
            }

            if(!hourDaySeparator[0].equals(day4) && m < nbTemperatureByDay && l == nbTemperatureByDay){
                bulletin.set_day5(reformatHour(hourDaySeparator[1]), value);
                m++;
            }

            if(!hourDaySeparator[0].equals(day5) && n < nbTemperatureByDay && m == nbTemperatureByDay){
                bulletin.set_day6(reformatHour(hourDaySeparator[1]), value);
                n++;
            }
        }
    }

    /**
     * changes the format of the string in parameter. Switch from a string of the form "18:00:00" to a string of the form "18" to keep only the first or the first 2 digits
     * @param hour The string to reformat
     * @return The string reformatted
     */
    private String reformatHour(String hour) {

            String [] reformatHour = hour.split(":"); // separates the first 2 digits of the hour
            String finalHour;
            if (reformatHour[0].substring(0,1).equals("0")){ // If the time starts with a zero, we take it off. (05 -> 5)
                finalHour = reformatHour[0].substring(1, 2);
            }
            else {
                finalHour = reformatHour[0]; }

        return finalHour;
    }
}
