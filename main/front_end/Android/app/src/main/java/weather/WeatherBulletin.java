package weather;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * The WeatherBulletin class contains the information from the weather report. It's filled in from the json sent by the server.
 */

public class WeatherBulletin {

    private String _cityName;
    private String _sentence;
    private double _temp;
    private String _error;
    /**
     * ArrayList containing the essential warnings sent by the server.
     * A warning is an error that does not prevent the application from working but can lead to false information.
     * It is a sentence indicating to the user the nature of the error.
     */
    private ArrayList<String> _warnings = new ArrayList<>();
    /**
     * Contains temperatures for the next 24 hours (every 3 hours) in the format <"Hour", temperature>
     */
    private LinkedHashMap<String, Double> _first24hour;
    /**
     * Contains the temperatures (every 3 hours) of day 2 (the next day) in the format <"Hour", temperature>
     */
    private LinkedHashMap<String, Double> _day2;
    /**
     * Contains the temperatures (every 3 hours) of day 3 in the format <"Hour", temperature>
     */
    private LinkedHashMap<String, Double> _day3;
    /**
     * Contains the temperatures (every 3 hours) of day 4 in the format <"Hour", temperature>
     */
    private LinkedHashMap<String, Double> _day4;
    /**
     * Contains the temperatures (every 3 hours) of day 5 in the format <"Hour", temperature>
     */
    private LinkedHashMap<String, Double> _day5;
    /**
     * Contains the temperatures (every 3 hours) of day 6 in the format <"Hour", temperature>
     */
    private LinkedHashMap<String, Double> _day6;
    /**
     * Contains the average temperatures by day in the format <"Name of the day", temperature>
     */
    private LinkedHashMap<String, Double> _averageDayWeather;
    /**
     * Contains the minimal temperatures by day in the format <"Name of the day", temperature>
     */
    private LinkedHashMap<String, Double> _temperatureMinByDay;
    /**
     * Contains the maximal temperatures by day in the format <"Name of the day", temperature>
     */
    private LinkedHashMap<String, Double> _temperatureMaxByDay;

    public WeatherBulletin(){
        _first24hour = new LinkedHashMap<>();
        _day2 = new LinkedHashMap<>();
        _day3 = new LinkedHashMap<>();
        _day4 = new LinkedHashMap<>();
        _day5 = new LinkedHashMap<>();
        _day6 = new LinkedHashMap<>();
        _averageDayWeather = new LinkedHashMap<>();
        _temperatureMinByDay = new LinkedHashMap<>();
        _temperatureMaxByDay = new LinkedHashMap<>();

    }

    public LinkedHashMap<String, Double> get_temperatureMinByDay(){return  _temperatureMinByDay;}

    public void set_temperatureMinByDay(String key, double value) {
       _temperatureMinByDay.put(key, value);
    }

    public  LinkedHashMap<String, Double> get_temperatureMaxByDay() { return  _temperatureMaxByDay; }

    public void set_temperatureMaxByDay(String key, double value) {
        _temperatureMaxByDay.put(key, value);
    }

    public LinkedHashMap<String, Double> get_first24hour() {
        return _first24hour;
    }

    public void set_first24hour(String key, double value) {
      _first24hour.put(key, value);
    }

    public LinkedHashMap<String, Double> get_day2() {
        return _day2;
    }

    public void set_day2(String key, double value) {
        _day2.put(key, value);
    }

    public LinkedHashMap<String, Double> get_day3() {
        return _day3;
    }

    public void set_day3(String key, double value) {
        _day3 .put(key, value);
    }

    public LinkedHashMap<String, Double> get_day4() {
        return _day4;
    }

    public void set_day4(String key, double value) {
        _day4.put(key, value);
    }

    public LinkedHashMap<String, Double> get_day5() {
        return _day5;
    }

    public void set_day5(String key, double value) {
        _day5.put(key, value);
    }

    public LinkedHashMap<String, Double> get_day6() {
        return _day6;
    }

    public void set_day6(String key, double value) {
        _day6.put(key, value);
    }

    public String get_sentence() {
        return _sentence;
    }

    public void set_sentence(String _sentence) {
        this._sentence = _sentence;
    }

    public double get_temp() {
        return _temp;
    }

    public void set_temp(double _temp) {
        this._temp = _temp;
    }

    public String get_error() {
        return _error;
    }

    public void set_error(String _error) {
        this._error = _error;
    }

    public ArrayList<String> get_warnings() {
        if (!_warnings.isEmpty())
        return _warnings;
        else return  null;
    }

    public void set_warnings(String warning) {
        _warnings.add(warning);
    }

    public LinkedHashMap<String, Double> get_averageDayWeather() {
        return _averageDayWeather;
    }

    public void set_averageDayWeather(String key, Double value) {
        _averageDayWeather.put(key, value);
    }

    public String get_cityName(){ return _cityName;}

    public void set_cityName(String _cityName){ this._cityName = _cityName; }

}
