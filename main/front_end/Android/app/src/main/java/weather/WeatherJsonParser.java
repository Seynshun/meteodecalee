package weather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;



/**
 * The WeatherJsonParser class parses the weather data sent by the server.
 */
public class WeatherJsonParser implements WeatherParser {
    WeatherBulletin bulletin;

    /**
     * Create a bulletin and store the data received by json in it
     *
     * @param json string format sent by the server
     */
    public WeatherJsonParser(String json) {
        bulletin = new WeatherBulletin();
        setTodayWeatherData(json); // today's data
        setWeatherForecastData(json); // forecast data
        setErrorAndWarnings(json); // errors and warnings data
    }

    public WeatherJsonParser() {
    }

    /**
     * Parse today's information and stores the results in a WeatherBulletin
     *
     * @param json string to parse
     */
    public void setTodayWeatherData(String json) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONObject today = jsonObject.getJSONObject("today");
            bulletin.set_temp(today.getDouble("temperature"));
            bulletin.set_sentence(today.getString("sentence"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Parse the weather forecast and stores the results in a WeatherBulletin
     *
     * @param json string to parse
     */
    public void setWeatherForecastData(String json) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONObject main = jsonObject.getJSONObject("main");
            int nbLinesList1 = main.getInt("nbForecast");
            int nbLinesList2 = main.getInt("nbForecastList2");

            LinkedHashMap<String, Double> _hourlyWeather = new LinkedHashMap<>();

            JSONArray average_temperature_by_day = (JSONArray) jsonObject.getJSONArray("list");
            JSONArray hourly_temperature = (JSONArray) jsonObject.getJSONArray("list2");

            for (int i = 0; i <= nbLinesList1; i++) { // loop through the jsonArray containing min, max and average temperature per day
                JSONObject dayWeather = average_temperature_by_day.getJSONObject(i);
                String date = dayWeather.getString("date");
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                Date dt1 = format1.parse(date);
                DateFormat format2 = new SimpleDateFormat("EEEE");
                String finalDay = format2.format(dt1);
                double temperature = dayWeather.getDouble("temperature");
                double minTemperature = dayWeather.getDouble("temperature_min");
                double maxTemperature = dayWeather.getDouble("temperature_max");
                final String key = finalDay.substring(0, 1).toUpperCase() + finalDay.substring(1);
                bulletin.set_temperatureMaxByDay(key, maxTemperature);
                bulletin.set_averageDayWeather(key, temperature);
                bulletin.set_temperatureMinByDay(key, minTemperature);

            }

            for (int i = 0; i < nbLinesList2; i++) { // loop through the jsonArray containing hourly forecast temperature

                JSONObject hourlyWeather = hourly_temperature.getJSONObject(i);
                String date = hourlyWeather.getString("date");

                double temperature = hourlyWeather.getDouble("temperature");
                _hourlyWeather.put(date, temperature);

            }
            ForecastSort sort = new ForecastSort();
            sort.daySort(_hourlyWeather, bulletin);
            bulletin.set_cityName(main.getString("city"));
        }catch (JSONException | ParseException e){
            e.printStackTrace();
        }

    }

    /**
     * Parse potential errors and warnings sent by the server and stores the results in a WeatherBulletin
     *
     * @param json string to parse
     */
    public void setErrorAndWarnings(String json){
        JSONObject jsonObject = null;

        try {

            jsonObject = new JSONObject(json);
            if (jsonObject.has("error")) {
                bulletin.set_error(jsonObject.getString("error"));
            }

            JSONObject main = jsonObject.getJSONObject("main");
            if (main.has("nbWarnings")) {
                int nbWarnings = main.getInt("nbWarnings");
                JSONArray warnings = (JSONArray) jsonObject.getJSONArray("warnings");
                for (int i = 0; i < nbWarnings; i++) {
                    JSONObject warning_field = warnings.getJSONObject(i);
                    String warningSentence = warning_field.getString("warning" + i);
                    bulletin.set_warnings(warningSentence);
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    /**
     * Create a json file with the GPS coordinates of the smartphone to send it to the server.
     *
     * @param longitude
     * @param latitude
     * @return The json file with the coordinates
     */
    public JSONObject toJson(double longitude, double latitude) throws JSONException {
        JSONObject json = new JSONObject();

        json.put("latitude", latitude);
        json.put("longitude", longitude);
        return json;
    }

    public WeatherBulletin getBulletin() {
        return bulletin;
    }

}