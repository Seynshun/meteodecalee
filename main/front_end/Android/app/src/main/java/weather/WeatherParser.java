package weather;

import org.json.JSONException;

import java.text.ParseException;

public interface WeatherParser {
    public void setTodayWeatherData(String fileToParse) throws JSONException;
    public void setWeatherForecastData(String fileToParse) throws JSONException, ParseException;
    public void setErrorAndWarnings(String fileToParse) throws JSONException;
}
