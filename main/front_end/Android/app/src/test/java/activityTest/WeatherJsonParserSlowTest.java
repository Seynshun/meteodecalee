package activityTest;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import weather.WeatherBulletin;
import weather.WeatherJsonParser;

import static org.junit.Assert.assertEquals;

/**
 * The WeatherJsonParserUnitTest class contains the integration tests of the JsonParser class
 */
public class WeatherJsonParserSlowTest {

    /**
     * Parse a json of the same type sent by the server.
     * @result WeatherBulletin data must match the expected data
     */
    @Test
    public void testWeatherJsonParser() {

        String jsonTest = "{\"today\":{\"sentence\":\"  Ce soir tu vas être content , on va avoir la raie en sauce .  \",\"temperature\":8.79},\"list2\":[{\"date\":\"2020-03-29 18:00:00\",\"temperature\":8.29},{\"date\":\"2020-03-29 21:00:00\",\"temperature\":6.01},{\"date\":\"2020-03-30 00: 00: 00\",\"temperature\":3.65},{\"date\":\"2020-03-30 03: 00: 00\",\"temperature\":2.8},{\"date\":\"2020-03-30 06: 00: 00\",\"temperature\":2.42},{\"date\":\"2020-03-30 09: 00: 00\",\"temperature\":3.76},{\"date\":\"2020-03-30 12: 00: 00\",\"temperature\":4.86},{\"date\":\"2020-03-30 15: 00: 00\",\"temperature\":5.12},{\"date\":\"2020-03-30 18: 00: 00\",\"temperature\":4.29},{\"date\":\"2020-03-30 21: 00: 00\",\"temperature\":4.03},{\"date\":\"2020-03-31 00:00:00\",\"temperature\":3.97},{\"date\":\"2020-03-31 03:00:00\",\"temperature\":3.08},{\"date\":\"2020-03-31 06:00:00\",\"temperature\":1.77},{\"date\":\"2020-03-31 09:00:00\",\"temperature\":6.04},{\"date\":\"2020-03-31 12: 00: 00\",\"temperature\":10.62},{\"date\":\"2020-03-31 15: 00: 00\",\"temperature\":12.25},{\"date\":\"2020-03-31 18: 00: 00\",\"temperature\":10.27},{\"date\":\"2020-03-31 21: 00: 00\",\"temperature\":7.84},{\"date\":\"2020-04-01 00: 00: 00\",\"temperature\":6.67},{\"date\":\"2020-04-01 03: 00: 00\",\"temperature\":6.3},{\"date\":\"2020-04-01 06: 00: 00\",\"temperature\":6.78},{\"date\":\"2020-04-01 09: 00: 00\",\"temperature\":12.8},{\"date\":\"2020-04-01 12: 00: 00\",\"temperature\":16.65},{\"date\":\"2020-04-01 15: 00: 00\",\"temperature\":17.02},{\"date\":\"2020-04-01 18: 00: 00\",\"temperature\":13.25},{\"date\":\"2020-04-01 21: 00: 00\",\"temperature\":9.12},{\"date\":\"2020-04-02 00:00:00\",\"temperature\":7.66},{\"date\":\"2020-04-02 03:00:00\",\"temperature\":5.93},{\"date\":\"2020-04-02 06:00:00\",\"temperature\":5.04},{\"date\":\"2020-04-02 09: 00: 00\",\"temperature\":10.77},{\"date\":\"2020-04-02 12: 00: 00\",\"temperature\":15.11},{\"date\":\"2020-04-02 15:00:00\",\"temperature\":15.14},{\"date\":\"2020-04-02 18:00:00\",\"temperature\":11.39},{\"date\":\"2020-04-02 21: 00: 00\",\"temperature\":7.6},{\"date\":\"2020-04-03 00: 00: 00\",\"temperature\":5.43},{\"date\":\"2020-04-03 03: 00: 00\",\"temperature\":3.68},{\"date\":\"2020-04-03 06:00:00\",\"temperature\":3.28},{\"date\":\"2020-04-03 09:00:00\",\"temperature\":9.33},{\"date\":\"2020-04-03 12:00:00\",\"temperature\":12.04},{\"date\":\"2020-04-03 15:00:00\",\"temperature\":12.47}],\"main\":{\"nbForecast\":5,\"city\":\"Talence\",\"nbForecastList2\":40,\"latitude\":44.8,\"longitude\":-0.6},\"list\":[{\"date\":\"2020-3-29\",\"sentence\":\"  Ce soir tu vas être content , on va avoir la raie en sauce .  \",\"city\":\"Talence\",\"temperature_min\":8,\"latitude\":44.8,\"temperature\":8.79,\"temperature_max\":10,\"longitude\":-0.6},{\"date\":\"2020-03-30\",\"sentence\":\"  Demain tu vas te frotterles mains , on va avoir les rillettes sous les bras .  \",\"city\":\"Talence\",\"temperature_min\":5.01,\"latitude\":44.8048,\"temperature\":7.1499999999999995,\"temperature_max\":8.29,\"longitude\":-0.5954},{\"date\":\"2020-03-31\",\"sentence\":\"  ce Mardi 31 Mars tu vas faire la gueule parce que on va se geler les roubignoles .  \",\"city\":\"Talence\",\"temperature_min\":2.42,\"latitude\":44.8048,\"temperature\":3.86625,\"temperature_max\":5.12,\"longitude\":-0.5954},{\"date\":\"2020-04-01\",\"sentence\":\"  ce Mercredi 1 Avril tu vas faire la tronche parce que on va se geler les roubignoles .  \",\"city\":\"Talence\",\"temperature_min\":1.77,\"latitude\":44.8048,\"temperature\":6.98,\"temperature_max\":12.25,\"longitude\":-0.5954},{\"date\":\"2020-04-02\",\"sentence\":\"  ce Jeudi 2 Avril tu vas péter joie , il va faire plus chaud que sous l'aiselle d'un tondeur .  \",\"city\":\"Talence\",\"temperature_min\":6.3,\"latitude\":44.8048,\"temperature\":11.07375,\"temperature_max\":17.02,\"longitude\":-0.5954},{\"date\":\"2020-04-03\",\"sentence\":\"  ce Vendredi 3 Avril tu vas être à les anges parce que on va avoir les olives qui baignent dans l'huile .  \",\"city\":\"Talence\",\"temperature_min\":5.04,\"latitude\":44.8048,\"temperature\":9.829999999999998,\"temperature_max\":15.14,\"longitude\":-0.5954}]}";
        WeatherJsonParser parser = new WeatherJsonParser(jsonTest);
        WeatherBulletin bulletin = parser.getBulletin();

        // setTodayWeatherData test
        assertEquals(8.79, bulletin.get_temp(), 0);
        assertEquals("  Ce soir tu vas être content , on va avoir la raie en sauce .  ", bulletin.get_sentence());

        // setWeatherForecastData test
        LinkedHashMap<String, Double> maxTemperatureExpected = new LinkedHashMap<>();
        maxTemperatureExpected.put("Dimanche", 10.0);
        maxTemperatureExpected.put("Lundi", 8.29);
        maxTemperatureExpected.put("Mardi", 5.12);
        maxTemperatureExpected.put("Mercredi", 12.25);
        maxTemperatureExpected.put("Jeudi", 17.02);
        maxTemperatureExpected.put("Vendredi", 15.14);

        LinkedHashMap<String, Double> minTemperatureExpected = new LinkedHashMap<>();
        minTemperatureExpected.put("Dimanche", 8.0);
        minTemperatureExpected.put("Lundi", 5.01);
        minTemperatureExpected.put("Mardi", 2.42);
        minTemperatureExpected.put("Mercredi", 1.77);
        minTemperatureExpected.put("Jeudi", 6.3);
        minTemperatureExpected.put("Vendredi", 5.04);

        LinkedHashMap<String, Double> temperatureExpected = new LinkedHashMap<>();
        temperatureExpected.put("Dimanche", 8.79);
        temperatureExpected.put("Lundi", 7.1499999999999995);
        temperatureExpected.put("Mardi", 3.86625);
        temperatureExpected.put("Mercredi", 6.98);
        temperatureExpected.put("Jeudi", 11.07375);
        temperatureExpected.put("Vendredi", 9.829999999999998);


        assertEquals(temperatureExpected, bulletin.get_averageDayWeather());
        assertEquals(minTemperatureExpected, bulletin.get_temperatureMinByDay());
        assertEquals(maxTemperatureExpected, bulletin.get_temperatureMaxByDay());
        assertEquals("Talence", bulletin.get_cityName());

        // setErrorAndWarnings test
        assertEquals(null, bulletin.get_error());
        assertEquals(null, bulletin.get_warnings());

    }

    /**
     * Parse a json of the same type sent by the server containing warnings
     * @result WeatherBulletin warnings must match the expected result
     */
    @Test
    public void testWeatherJsonParserWithWarnings() {

        String jsonTest = "{\"warnings\":[{\"warning0\":\"Nous ne pouvons communiquer avec l'API 'MeteoStat'\"}],\"today\":{\"sentence\":\"Aujourd'hui on va avoir les rillettes sous les bras .\",\"temperature\":17.77},\"list2\":[{\"date\":\"2020-04-03 15: 00: 00\",\"temperature\":18.36},{\"date\":\"2020-04-03 18:00:00\",\"temperature\":15.29},{\"date\":\"2020-04-03 21:00:00\",\"temperature\":12.79},{\"date\":\"2020-04-04 00: 00: 00\",\"temperature\":10.87},{\"date\":\"2020-04-04 03: 00: 00\",\"temperature\":9.25},{\"date\":\"2020-04-04 06: 00: 00\",\"temperature\":8.64},{\"date\":\"2020-04-04 09: 00: 00\",\"temperature\":12.58},{\"date\":\"2020-04-04 12: 00: 00\",\"temperature\":14.5},{\"date\":\"2020-04-04 15: 00: 00\",\"temperature\":14.61},{\"date\":\"2020-04-04 18: 00: 00\",\"temperature\":12.32},{\"date\":\"2020-04-04 21: 00: 00\",\"temperature\":11.35},{\"date\":\"2020-04-05 00: 00: 00\",\"temperature\":10.97},{\"date\":\"2020-04-05 03: 00: 00\",\"temperature\":9.64},{\"date\":\"2020-04-05 06: 00: 00\",\"temperature\":9.15},{\"date\":\"2020-04-05 09: 00: 00\",\"temperature\":13.19},{\"date\":\"2020-04-05 12: 00: 00\",\"temperature\":14.75},{\"date\":\"2020-04-05 15: 00: 00\",\"temperature\":14.67},{\"date\":\"2020-04-05 18: 00: 00\",\"temperature\":12.92},{\"date\":\"2020-04-05 21: 00: 00\",\"temperature\":11.99},{\"date\":\"2020-04-06 00: 00: 00\",\"temperature\":11.57},{\"date\":\"2020-04-06 03: 00: 00\",\"temperature\":11.4},{\"date\":\"2020-04-06 06: 00: 00\",\"temperature\":9.93},{\"date\":\"2020-04-06 09:00:00\",\"temperature\":13.71},{\"date\":\"2020-04-06 12:00:00\",\"temperature\":14.74},{\"date\":\"2020-04-06 15:00: 00\",\"temperature\":14.4},{\"date\":\"2020-04-06 18: 00: 00\",\"temperature\":12.83},{\"date\":\"2020-04-06 21: 00: 00\",\"temperature\":12.17},{\"date\":\"2020-04-07 00:00:00\",\"temperature\":11.5},{\"date\":\"2020-04-07 03:00:00\",\"temperature\":11.5},{\"date\":\"2020-04-07 06: 00: 00\",\"temperature\":11.47},{\"date\":\"2020-04-07 09: 00: 00\",\"temperature\":14.37},{\"date\":\"2020-04-07 12: 00: 00\",\"temperature\":16.23},{\"date\":\"2020-04-07 15: 00: 00\",\"temperature\":16.32},{\"date\":\"2020-04-07 18: 00: 00\",\"temperature\":14.55},{\"date\":\"2020-04-07 21:00:00\",\"temperature\":13.41},{\"date\":\"2020-04-08 00:00:00\",\"temperature\":12.65},{\"date\":\"2020-04-08 03: 00: 00\",\"temperature\":11.96},{\"date\":\"2020-04-08 06: 00: 00\",\"temperature\":11.77},{\"date\":\"2020-04-08 09: 00: 00\",\"temperature\":15.19},{\"date\":\"2020-04-08 12: 00: 00\",\"temperature\":16.73}],\"main\":{\"nbWarnings\":1,\"nbForecast\":5,\"city\":\"Montpellier\",\"nbForecastList2\":40,\"latitude\":43.61,\"longitude\":3.88},\"list\":[{\"date\":\"2020-4-3\",\"sentence\":\"Aujourd'hui on va avoir les rillettes sous les bras .\",\"city\":\"Montpellier\",\"temperature_min\":17.22,\"latitude\":43.61,\"temperature\":17.77,\"temperature_max\":18.33,\"longitude\":3.88},{\"date\":\"2020-04-04\",\"sentence\":\"Demain on va avoir les bigorneaux qui collent aux rochers .\",\"city\":\"Montpellier\",\"temperature_min\":11.16,\"latitude\":43.6109,\"temperature\":15.479999999999999,\"temperature_max\":18.36,\"longitude\":3.8772},{\"date\":\"2020-04-05\",\"sentence\":\"ce Dimanche 5 Avril on va crever .\",\"city\":\"Montpellier\",\"temperature_min\":8.64,\"latitude\":43.6109,\"temperature\":11.764999999999997,\"temperature_max\":14.61,\"longitude\":3.8772},{\"date\":\"2020-04-06\",\"sentence\":\"ce Lundi 6 Avril on va crever de chaud .\",\"city\":\"Montpellier\",\"temperature_min\":9.15,\"latitude\":43.6109,\"temperature\":12.159999999999998,\"temperature_max\":14.75,\"longitude\":3.8772},{\"date\":\"2020-04-07\",\"sentence\":\"ce Mardi 7 Avril on va avoir la raie en sauce .\",\"city\":\"Montpellier\",\"temperature_min\":9.93,\"latitude\":43.6109,\"temperature\":12.59375,\"temperature_max\":14.74,\"longitude\":3.8772},{\"date\":\"2020-04-08\",\"sentence\":\"ce Mercredi 8 Avril on va avoir la raie en sauce .\",\"city\":\"Montpellier\",\"temperature_min\":11.47,\"latitude\":43.6109,\"temperature\":13.668749999999998,\"temperature_max\":16.32,\"longitude\":3.8772}]}";
        WeatherJsonParser parser = new WeatherJsonParser(jsonTest);
        WeatherBulletin bulletin = parser.getBulletin();

        ArrayList<String> _warnings = new ArrayList<>();
        _warnings.add("Nous ne pouvons communiquer avec l'API 'MeteoStat'");
        assertEquals(_warnings, bulletin.get_warnings());
    }

    /**
     * Parse a json of the same type sent by the server containing an error
     * @result WeatherBulletin error must match the expected result
     */
    @Test
    public void testWeatherJsonParserWithErrors() {
        String jsonTest = "{\"main\":{},\"error\":\"Nous ne pouvons joindre Elvex\"}";
        WeatherJsonParser parser = new WeatherJsonParser(jsonTest);
        WeatherBulletin bulletin = parser.getBulletin();

        String errorExpected = "Nous ne pouvons joindre Elvex";
        assertEquals(errorExpected, bulletin.get_error());
    }
}
