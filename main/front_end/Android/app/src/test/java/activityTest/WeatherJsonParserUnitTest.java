package activityTest;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import weather.ForecastSort;
import weather.WeatherJsonParser;

/**
 * The WeatherJsonParserUnitTest class contains the unit tests of the JsonParser class
 */
public class WeatherJsonParserUnitTest {

    /**
     * Create a json with coordinates
     * @result The json created must have the expected format
     * @throws JSONException
     */
    @Test
    public void testToJson() throws JSONException {
        WeatherJsonParser parser = new WeatherJsonParser();

        JSONObject json = parser.toJson(10.12, 54.15);
        String jsonExpected = "{\"latitude\":54.15,\"longitude\":10.12}";
        assertEquals(jsonExpected, json.toString());
    }

    /**
     * Create a json with with a coordinate being an infinite number
     * @result Should raise an exception
     * @throws JSONException
     */
    @Test(expected = JSONException.class)
    public void testToJsonException() throws JSONException {
        WeatherJsonParser parser = new WeatherJsonParser();
        parser.toJson(Double.POSITIVE_INFINITY, 54.15);
    }

}
